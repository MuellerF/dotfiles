;; Emacs mode
;; (define-configuration buffer
;;   ((default-modes
;;     (pushnew 'nyxt/mode/emacs:emacs-mode %slot-value%))))

;; Vi Mode
(define-configuration buffer
  ((default-modes
    (pushnew 'nyxt/mode/vi:vi-normal-mode %slot-value%))))

(define-configuration browser
  (;; This is for Nyxt to never prompt me about restoring the previous session.
   (autofills (list (make-autofill :name "First Name" :fill "Florian")
                    (make-autofill :name "Last Name" :fill "Mueller")
                    (make-autofill :name "Name" :fill "Florian Mueller")
                    (make-autofill :name "Email" :fill "florian.mueller.unisg@gmail.com")))))

(defvar *my-search-engines*
  (list
   '("google" "https://google.com/search?q=~a" "https://google.com")
   '("python3" "https://docs.python.org/3/search.html?q=~a"
     "https://docs.python.org/3")
   '("doi" "https://dx.doi.org/~a" "https://dx.doi.org/")
  '("brave" "https://search.brave.com/search?q=~a" "https://search.brave.com/"))
  "List of search engines.")

(define-configuration context-buffer
  "Go through the search engines above and make-search-engine out of them."
  ((search-engines
    (append
     (mapcar (lambda (engine) (apply 'make-search-engine engine))
             *my-search-engines*)
     %slot-default%))))

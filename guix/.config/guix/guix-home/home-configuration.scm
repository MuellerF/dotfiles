;; This "home-environment" file can be passed to 'guix home reconfigure'
;; to reproduce the content of your profile.  This is "symbolic": it only
;; specifies package names.  To reproduce the exact same profile, you also
;; need to capture the channels being used, as returned by "guix describe".
;; See the "Replicating Guix" section in the manual.

(use-modules (gnu home)
             (gnu packages)
             (gnu packages shells)
             (gnu services)
             (guix gexp)
             (gnu home services shells))

(home-environment
 ;; Below is the list of packages that will show up in your
 ;; Home profile, under ~/.guix-home/profile.
 (packages (specifications->packages (list "emacs"
                                           "picom"
                                           "nvidia-driver"
                                           "setxkbmap"
                                           "firefox"
                                           "bluez"
                                           "xwallpaper"
                                           "rofi"
                                           "nyxt"
                                           "neofetch"
                                           "stow"
                                           "neovim"
                                           "freecad"
                                           "git"
                                           "alacritty"
                                           "libreoffice"
                                           )))

 ;; Below is the list of Home services.  To search for available
 ;; services, run 'guix home search KEYWORD' in a terminal.
 (services
  (list (service home-bash-service-type
                 (home-bash-configuration
                  (aliases '(("grep" . "grep --color=auto") ("lls" . "ls -l")
                             ("ls" . "ls -p --color=auto")))
                  (bashrc (list (local-file
                                 "/home/fm/.config/guix/guix-home//.bashrc"
                                 "bashrc")))
                  (bash-profile (list (local-file
                                       "/home/fm/.config/guix/guix-home//.bash_profile"
                                       "bash_profile")))))
        ;;TODO make zsh default shell
        ;; (simple-service 'some-useful-env-vars-service
        ;;                 home-environment-variables-service-type
        ;;                 `(
        ;;                   ("SHELL" . ,(file-append zsh "/bin/zsh"))
        ;;                   ))

        )))

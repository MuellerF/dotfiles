;; Indicate which modules to import to access the variables
;; used in this configuration.



(add-to-load-path "/home/fm/.config/guix")
(define-module (work)
  #:use-module (base-system)
  #:use-module (gnu))

(operating-system
 (inherit base-operating-system)
 (host-name "guix-work")
 (bootloader (bootloader-configuration
              (bootloader grub-efi-bootloader)
              (targets (list "/boot/efi"))
              ))

 ;; The list of file systems that get "mounted".  The unique
 ;; file system identifiers there ("UUIDs") can be obtained
 ;; by running 'blkid' in a terminal.
 (file-systems (cons* (file-system
                       (mount-point "/")
                       (device "/dev/nvme0n1p3")
                       (type "btrfs"))
                      (file-system
                       (mount-point "/boot/efi")
                       (device (uuid "7FEA-298B"
                                     'fat32))
                       (type "vfat")) %base-file-systems)))

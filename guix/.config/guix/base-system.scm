;; Indicate which modules to import to access the variables
;; used in this configuration.
(define-module (base-system)
  #:use-module (gnu)
  #:use-module (nongnu packages linux))

(use-service-modules cups desktop networking ssh linux xorg)

(define-public base-operating-system
  (operating-system
   (kernel linux)
   (firmware (list linux-firmware))
   (locale "en_GB.utf8")
   (timezone "Europe/Zurich")
   (keyboard-layout (keyboard-layout "ch" #:options '("ctrl:swapcaps_hyper")))
   (host-name "guix-fm")

   ;; The list of user accounts ('root' is implicit).
   (users (cons* (user-account
                  (name "fm")
                  (comment "Florian Mueller")
                  (group "users")
                  (home-directory "/home/fm")
                  (supplementary-groups '("wheel" "netdev" "audio" "video")))
                 %base-user-accounts))

   ;; Packages installed system-wide.  Users can also install packages
   ;; under their own account: use 'guix search KEYWORD' to search
   ;; for packages and 'guix install PACKAGE' to install a package.

   (packages (append
              (map specification->package
                   (list "gnupg"
                         "nss-certs"
                         "git"
                         "ripgrep"
                         "ntfs-3g"
                         "exfat-utils"
                         "fuse-exfat"
                         "stow"
                         "htop"
                         "emacs"
                         "setxkbmap"
                         "gcc-toolchain"
                         "glibc-locales"
                         "zsh-syntax-highlighting"
                         "zsh"
                         "sqlite"
                         ))
              %base-packages))
   ;; Below is the list of system services.  To search for available
   ;; services, run 'guix system search KEYWORD' in a terminal.
   (services
    (append (list

             ;; To configure OpenSSH, pass an 'openssh-configuration'
             ;; record as a second argument to 'service' below.
             (service openssh-service-type)
             (service cups-service-type)
             (set-xorg-configuration
              (xorg-configuration (keyboard-layout keyboard-layout))))

            ;; This is the default list of services we
            ;; are appending to.
            %desktop-services))

   (bootloader (bootloader-configuration
                (bootloader grub-efi-bootloader)
                (targets (list "/boot/efi"))
                (keyboard-layout keyboard-layout)))

   (file-systems (cons*
                  (file-system
                   (mount-point "/tmp")
                   (device "none")
                   (type "tmpfs")
                   (check? #f))
                  %base-file-systems))
   ))

(add-to-load-path "/home/fm/.config/guix")
(define-module (desktop)
  #:use-module (base-system)  ; Import the base-system module
  #:use-module (gnu))

   (use-service-modules cups desktop networking ssh xorg)

   (operating-system
    (inherit base-operating-system)


     (mapped-devices (list (mapped-device
                            (source (uuid
                                     "aead92c0-30be-4425-84c1-dcce9ddc76dc"))
                            (target "cryptroot")
                            (type luks-device-mapping))))
     (file-systems (cons* (file-system
                           (mount-point "/")
                           (device "/dev/mapper/cryptroot")
                           (type "btrfs")
                           (dependencies mapped-devices))
                          (file-system
                           (mount-point "/boot/efi")
                           (device (uuid "BB41-D161"
                                         'fat32))
                           (type "vfat")) %base-file-systems))

    )

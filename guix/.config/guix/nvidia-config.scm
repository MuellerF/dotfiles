;; Indicate which modules to import to access the variables
  ;; used in this configuration.
  (use-modules (gnu)(gnu services) (gnu services networking)(nongnu packages linux) (guix transformations)(nongnu packages nvidia))
  (use-service-modules desktop networking ssh xorg)
(use-service-modules linux)

  ;;Nvidia fuckery
  (define transform
    (options->transformation
     '((with-graft . "mesa=nvda"))))

  (operating-system
    (kernel linux)
    (firmware (list linux-firmware))
    (kernel-loadable-modules (list nvidia-module))
    (locale "en_GB.utf8")
    (timezone "Europe/Zurich")
    (keyboard-layout (keyboard-layout "ch"))
    (host-name "guix-fm")

    ;; The list of user accounts ('root' is implicit).
    (users (cons* (user-account
                    (name "fm")
                    (comment "Florian Mueller")
                    (group "users")
                    (home-directory "/home/fm")
                    (supplementary-groups '("wheel" "netdev" "audio" "video")))
                  %base-user-accounts))

    ;; Packages installed system-wide.  Users can also install packages
    ;; under their own account: use 'guix search KEYWORD' to search
    ;; for packages and 'guix install PACKAGE' to install a package.
    (packages (append
               (map specification->package
                    (list "awesome"
                          "nss-certs"
                          "git"
                          "password-store"
                          "nvidia-module"
                          "nvidia-driver"
                            ))
                      %base-packages))



    ;; Below is the list of system services.  To search for available
    ;; services, run 'guix system search KEYWORD' in a terminal.
    (services
     (append (list

                   ;; To configure OpenSSH, pass an 'openssh-configuration'
                   ;; record as a second argument to 'service' below.
                   (service openssh-service-type)
                   ;;Load nvidia services
                   (simple-service 
                       'custom-udev-rules udev-service-type 
                       (list nvidia-driver))
                   (service kernel-module-loader-service-type
                               '("ipmi_devintf"
                                 "nvidia"
                                 "nvidia_modeset"
                                 "nvidia_uvm"))
                   ;;xorg
                   (set-xorg-configuration
                    (xorg-configuration (keyboard-layout keyboard-layout)
                                  (modules (cons* nvidia-driver %default-xorg-modules))
                                  (drivers '("nvidia")))))
                   ))

    ;;blacklisting nouveau drivers to enabl nvidia drivers to work
    (kernel-arguments (append
                     '("modprobe.blacklist=nouveau")
                     %default-kernel-arguments))

    (bootloader (bootloader-configuration
                  (bootloader grub-efi-bootloader)
                  (targets (list "/boot/efi"))
                  (keyboard-layout keyboard-layout)))
    (mapped-devices (list (mapped-device
                            (source (uuid
                                     "aead92c0-30be-4425-84c1-dcce9ddc76dc"))
                            (target "cryptroot")
                            (type luks-device-mapping))))

    ;; The list of file systems that get "mounted".  The unique
    ;; file system identifiers there ("UUIDs") can be obtained
    ;; by running 'blkid' in a terminal.
    (file-systems (cons* (file-system
                           (mount-point "/")
                           (device "/dev/mapper/cryptroot")
                           (type "btrfs")
                           (dependencies mapped-devices))
                         (file-system
                           (mount-point "/boot/efi")
                           (device (uuid "BB41-D161"
                                         'fat32))
                           (type "vfat")) %base-file-systems)))

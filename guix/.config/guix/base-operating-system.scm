;; Indicate which modules to import to access the variables
;; used in this configuration.
(use-modules (gnu))
(use-service-modules cups desktop networking ssh xorg)
(define-public base-operating-system
  (operating-system
   (locale "en_GB.utf8")
   (timezone "Europe/Zurich")
   (keyboard-layout (keyboard-layout "ch" #:options '("ctrl:swapcaps_hyper")))
   (host-name "guix-fm")

   ;; The list of user accounts ('root' is implicit).
   (users (cons* (user-account
                  (name "fm")
                  (comment "Florian Mueller")
                  (group "users")
                  (home-directory "/home/fm")
                  (supplementary-groups '("wheel" "netdev" "audio" "video")))
                 %base-user-accounts))

   ;; Packages installed system-wide.  Users can also install packages
   ;; under their own account: use 'guix search KEYWORD' to search
   ;; for packages and 'guix install PACKAGE' to install a package.

   (packages (append
              (map specification->package
                   (list "awesome"
                         "nss-certs"
                         "git"
                         "ntfs-3g"
                         "exfat-utils"
                         "fuse-exfat"
                         "stow"
                         "vim"
                         "emacs"
                         ))
              %base-packages))
   ;; Below is the list of system services.  To search for available
   ;; services, run 'guix system search KEYWORD' in a terminal.
   (services
    (append (list

             ;; To configure OpenSSH, pass an 'openssh-configuration'
             ;; record as a second argument to 'service' below.
             (service openssh-service-type)
             (service cups-service-type)
             (set-xorg-configuration
              (xorg-configuration (keyboard-layout keyboard-layout))))

            ;; This is the default list of services we
            ;; are appending to.
            %desktop-services))
   (bootloader (bootloader-configuration
                (bootloader grub-efi-bootloader)
                (targets (list "/boot/efi"))
                (keyboard-layout keyboard-layout)))
   (mapped-devices (list (mapped-device
                          (source (uuid
                                   "aead92c0-30be-4425-84c1-dcce9ddc76dc"))
                          (target "cryptroot")
                          (type luks-device-mapping))))

   ;; The list of file systems that get "mounted".  The unique
   ;; file system identifiers there ("UUIDs") can be obtained
   ;; by running 'blkid' in a terminal.
   (file-systems (cons* (file-system
                         (mount-point "/")
                         (device "/dev/mapper/cryptroot")
                         (type "btrfs")
                         (dependencies mapped-devices))
                        (file-system
                         (mount-point "/boot/efi")
                         (device (uuid "BB41-D161"
                                       'fat32))
                         (type "vfat")) %base-file-systems))))


local awful = require("awful")
-- local naughty = require("naughty")
-- local watch = require("awful.widget.watch")
local wibox = require("wibox")
-- local gfs = require("gears.filesystem")
-- local dpi = require('beautiful').xresources.apply_dpi
local beautiful = require("beautiful")

local volume_text_widget = wibox.widget{
	widget = wibox.widget.textbox
}

--By using the wibox.container.background widget, we can specify colours for fg and bg
local volume_widget = wibox.widget {
--	layout = wibox.layout.fixed.horizontal,
	 volume_text_widget,
	-- {widget = wibox.widget.textbox,
	-- text = },
	widget = wibox.container.background
}

--The awful.widget.watch executes the command given to it as the first argument in an interval specified by the second argument. 
--In this case, it executes a bahs command, which returns information on the status of the Master Ausio channel(volume, etc.). 
--The third argument specifies the execution of commands on the output of the awful.widget.watch. 
--We can address the output of the bash comTmand through the second argument given the the function in the third argument, in this case "stdout".
--It will only work in  the second position, by default the first is occupied by "widget", no idea what that does.
--in this example, we use a for loop to examine if any of the returned lines match with the pattern specified within quotations.
--The percent character acts as special character, signifiying that the following character acts as a variable of sorts. 
--%d=any digit, %d+ = any number of digits, %% = percent sign, %[ = [(%->escape character)
--Once the desired text is isolated, we store it in the variable volume. 
--We then access the previously created widget "volume_text_widget", specifically its "text"-field, and assign it the newly created "volume"-variable.

awful.widget.watch('bash -c "amixer sget Master"', 60, function (widget,stdout)
		for line in stdout:gmatch("[^\r\n]+") do
			if line:match("%[(%d+%%)%]") then
			local volume =  string.match (line, "%[(%d+)%%%]")
			if tonumber(volume) > 90 then
				volume_widget.fg = beautiful.fg_urgent
			end
			volume_text_widget.text = volume..'%'
			end
		end
	end)

return volume_widget

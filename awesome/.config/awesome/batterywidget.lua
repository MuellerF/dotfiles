local awful = require("awful")
-- local naughty = require("naughty")
-- local watch = require("awful.widget.watch")
local wibox = require("wibox")
-- local gfs = require("gears.filesystem")
-- local dpi = require('beautiful').xresources.apply_dpi
local beautiful = require("beautiful")




	--Sample output: "Battery 0: Discharging, 61%, 01:10:12 remaining"
--Matches the string of acpioutput to a puntuation mark (=%p), a space (=%s) an captures all the following letters, represented by the %a for letters and the + to the following one to infinity ocurrences of the character.
--local batterystatus =  string.match (acpioutput, "%p%s(%a+)")

--Matches the string of acpioutput to two digits (=%d%d) and a percentsign (=%%)
-- local batterypercentage =  string.match (acpioutput, "%d%d%%")

local battery_widget_text = wibox.widget{
    widget = wibox.widget.textbox
}

local battery_widget = wibox.widget{
	battery_widget_text,
	widget = wibox.container.background
}

awful.widget.watch('bash -c "acpi"', 60, function (widget, stdout)
		local percentage_table = {}
		for line in stdout:gmatch("[^\r\n]+") do
			table.insert(percentage_table, string.match (line, "(%d+)%%"))
		end
		local batterypercentage = (percentage_table[1] + percentage_table[2])/2
			if tonumber(batterypercentage) < 25 then
			battery_widget.fg = beautiful.bg_urgent
			end
		local batterystatus =  string.match (stdout, "%p%s(%a+)")
			if batterystatus == "Charging" then
				battery_widget.fg = beautiful.fg_focus
			end
		battery_widget_text.text = batterypercentage..'%'--batterystatus ..','.. batterypercentage
	end)

 return battery_widget

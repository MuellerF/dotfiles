(setenv "PATH" (concat (getenv "PATH") "~/.local/bin"))
(setq exec-path (append exec-path '("~/.local/bin")))

(defvar bootstrap-version)
(let ((bootstrap-file
       (expand-file-name "straight/repos/straight.el/bootstrap.el" user-emacs-directory))
      (bootstrap-version 5))
  (unless (file-exists-p bootstrap-file)
    (with-current-buffer
        (url-retrieve-synchronously
         "https://raw.githubusercontent.com/raxod502/straight.el/develop/install.el"
         'silent 'inhibit-cookies)
      (goto-char (point-max))
      (eval-print-last-sexp)))
  (load bootstrap-file nil 'nomessage))

(straight-use-package 'use-package)

  (setq straight-use-package-by-default t)

;; Prevent org versions from clashing
  (straight-use-package 'org)

(setq use-short-answers t)

(setq inhibit-startup-message t)
(scroll-bar-mode             -1)   ;       Disable visible scrollbar
(tool-bar-mode               -1)   ;       Disable the     toolbar
(tooltip-mode                -1)   ;       Disable tooltips
(set-fringe-mode             10)   ;       Give    some    breathing room
(menu-bar-mode               -1)   ;       Disable the     menu      bar
(tab-bar-mode                -1) ; Disable the     buffer  tab       bar on tob

(column-number-mode)
(global-display-line-numbers-mode t)

(dolist (mode '(org-mode-hook
                term-mode-hook
                shell-mode-hook
                treemacs-mode-hook
                pdf-view-mode-hook
                eshell-mode-hook))
  (add-hook mode (lambda () (display-line-numbers-mode 0))))

(use-package rainbow-delimiters
  :hook (prog-mode . rainbow-delimiters-mode))

(setq visible-bell t)

(set-face-attribute 'default nil :font "SauceCodePro Nerd Font" :height 135)
;; Set the fixed pitch face
(set-face-attribute 'fixed-pitch nil :font "SauceCodePro Nerd Font" :height 135)
;; Set the variable pitch face
(set-face-attribute 'variable-pitch nil :font "Cantarell" :height 140 :weight 'regular)

(use-package page-break-lines)

(use-package rainbow-mode
  :straight t)

(use-package treemacs
  :straight t
  :defer t
  :init
  (with-eval-after-load 'winum
    (define-key winum-keymap (kbd "M-0") #'treemacs-select-window))
  :config
  (progn
    (setq treemacs-collapse-dirs                   (if treemacs-python-executable 3 0)
          treemacs-deferred-git-apply-delay        0.5
          treemacs-directory-name-transformer      #'identity
          treemacs-display-in-side-window          t
          treemacs-eldoc-display                   t
          treemacs-file-event-delay                5000
          treemacs-file-extension-regex            treemacs-last-period-regex-value
          treemacs-file-follow-delay               0.2
          treemacs-file-name-transformer           #'identity
          treemacs-follow-after-init               t
          treemacs-expand-after-init               t
          treemacs-git-command-pipe                ""
          treemacs-goto-tag-strategy               'refetch-index
          treemacs-indentation                     2
          treemacs-indentation-string              " "
          treemacs-is-never-other-window           nil
          treemacs-max-git-entries                 5000
          treemacs-missing-project-action          'ask
          treemacs-move-forward-on-expand          nil
          treemacs-no-png-images                   nil
          treemacs-no-delete-other-windows         t
          treemacs-project-follow-cleanup          nil
          treemacs-persist-file                    (expand-file-name ".cache/treemacs-persist" user-emacs-directory)
          treemacs-position                        'left
          treemacs-read-string-input               'from-child-frame
          treemacs-recenter-distance               0.1
          treemacs-recenter-after-file-follow      nil
          treemacs-recenter-after-tag-follow       nil
          treemacs-recenter-after-project-jump     'always
          treemacs-recenter-after-project-expand   'on-distance
          treemacs-litter-directories              '("/node_modules" "/.venv" "/.cask")
          treemacs-show-cursor                     nil
          treemacs-show-hidden-files               t
          treemacs-silent-filewatch                nil
          treemacs-silent-refresh                  nil
          treemacs-sorting                         'alphabetic-asc
          treemacs-select-when-already-in-treemacs 'move-back
          treemacs-space-between-root-nodes        t
          treemacs-tag-follow-cleanup              t
          treemacs-tag-follow-delay                1.5
          treemacs-text-scale                      nil
          treemacs-user-mode-line-format           nil
          treemacs-user-header-line-format         nil
          treemacs-width                           35
          treemacs-width-is-initially-locked       t
          treemacs-workspace-switch-cleanup        nil)

    ;; The default width and height of the icons is 22 pixels. If you are
    ;; using a Hi-DPI display, uncomment this to double the icon size.
    ;;(treemacs-resize-icons 44)

    (treemacs-follow-mode t)
    (treemacs-filewatch-mode t)
    (treemacs-fringe-indicator-mode 'always)

    (pcase (cons (not (null (executable-find "git")))
                 (not (null treemacs-python-executable)))
      (`(t . t)
       (treemacs-git-mode 'deferred))
      (`(t . _)
       (treemacs-git-mode 'simple)))

    (treemacs-hide-gitignored-files-mode nil))
  :bind
  (:map global-map
        ("M-0"       . treemacs-select-window)
        ("C-x t 1"   . treemacs-delete-other-windows)
        ("C-x t t"   . treemacs)
        ("C-x t B"   . treemacs-bookmark)
        ("C-x t C-t" . treemacs-find-file)
        ("C-x t M-t" . treemacs-find-tag)))

(use-package treemacs-evil
  :after (treemacs evil))

                                        ;(use-package treemacs-projectile
                                        ;  :after (treemacs projectile)
                                        ;  :straight t)

                                        ;(use-package treemacs-icons-dired
                                        ;  :hook (dired-mode . treemacs-icons-dired-enable-once)
                                        ;  :straight t)

                                        ;(use-package treemacs-magit
                                        ;  :after (treemacs magit)
                                        ;  :straight t)

                                        ;(use-package treemacs-persp ;;treemacs-perspective if you use perspective.el vs. persp-mode
                                        ;  :after (treemacs persp-mode) ;;or perspective vs. persp-mode
                                        ;  :straight t
                                        ;  :config (treemacs-set-scope-type 'Perspectives))

(use-package undo-fu
  :straight t
  :bind (:map evil-normal-state-map
              ("u" . undo-fu-only-undo)
              ("\C-r" . undo-fu-only-redo))
  )

(use-package all-the-icons
  :straight t
  :if (display-graphic-p))

(use-package beacon
  :straight t
  :config  
  (beacon-mode 1))

(use-package eros
  :straight t
  :config (eros-mode 1))

(use-package f
  :straight t)

(use-package uuidgen
  :straight t)

(defun fm/sudo-edit (&optional arg)
  "Edit currently visited file as root.

With a prefix ARG prompt for a file to visit.
Will also prompt for a file to visit if current
buffer is not visiting a file."
  (interactive "P")
  (if (or arg (not buffer-file-name))
      (find-file (concat "/sudo:root@localhost:"
                         (ido-read-file-name "Find file(as root): ")))
    (find-alternate-file (concat "/sudo:root@localhost:" buffer-file-name))))

(defun fm/print-file ()
  "Prompt the user to enter a filename, number of copies, and one-sided or two-sided printing, then print the specified file."
  (interactive)
  (let* ((filename (read-file-name "Enter filename: "))
         (number (read-number "Enter number of copies: " 1))
         (sides-option (read-number "Enter 1 for one-sided or 2 for two-sided printing (default 2): " 2))
         (sides (if (= sides-option 1) "one-sided" "two-sided-long-edge")))
    (async-shell-command (concat "lpr " filename 
                                 " -# " (number-to-string number) 
                                 " -o sides=" sides))))
(defun fm/print-buffer ()
  "Prompt for number of copies and one-sided or two-sided printing, then print the current buffer."
  (interactive)
  (let* ((filename (buffer-file-name))
         (number (read-number "Enter number of copies: " 1))
         (sides-option (read-number "Enter 1 for one-sided or 2 for two-sided printing (default 2): " 2))
         (sides (if (= sides-option 1) "one-sided" "two-sided-long-edge")))
    (async-shell-command (concat "lpr " filename 
                                 " -# " (number-to-string number) 
                                 " -o sides=" sides))))

(defun fm/adf-scan-to-file ()
  "Prompt the user to enter a filename, then scan a document to that filename"
  (interactive)
  (let ((filename (read-file-name "Enter filename: ")))
    (async-shell-command (concat  "scanimage --device \"airscan:e0:Xerox(R) B225 MFP\" --format tiff --batch=$(date +%Y%m%d_%H%M%S)_p%04d_scan.tiff  --resolution 300 --source ADF && convert *.tiff " filename"-tmp && rm ./*scan.tiff && magick -density 168x168 -quality 76 -compress JPEG "filename"-tmp " filename".pdf && rm "filename"-tmp"))))

(defun fm/scan-to-file-single ()
  "Prompt the user to enter a filename, then scan a document to that filename"
  (interactive)
  (let ((filename (read-file-name "Enter filename: ")))
    (async-shell-command (concat  "scanimage --device \"airscan:e0:Xerox(R) B225 MFP\" --format=pdf --output-file "filename"-tmp.pdf --resolution 300 && magick -density 168x168 -quality 76 -compress JPEG "filename"-tmp.pdf " filename".pdf && rm "filename"-tmp.pdf"))))

(defun fm/duplex-scan-to-file ()
  "Prompt the user to enter a filename, then scan a double sided document to that filename"
  (interactive)
  (let ((filename (read-file-name "Enter filename: ")))
    (async-shell-command (concat  "~/.local/duplex-scan.sh " filename))))

(setq vc-follow-symlinks t)

(defun fm/read-file-name-with-default (prompt dir default)
  "Read a file name with a default value in the minibuffer."
  (let ((minibuffer-setup-hook
         (lambda ()
           (delete-minibuffer-contents)
           (insert (or default "")))))
    (read-file-name prompt dir default)))

(defun fm/merge-pdf-files()
  "Prompt the user to enter filenames and display the pdfunite command."
  (interactive)
  (let* ((buffer buffer-file-name)
         (input1 (fm/read-file-name-with-default "Enter the first input filename: " nil (if buffer (concat (file-name-sans-extension buffer) ".pdf") "~/")))
         (input2 (fm/read-file-name-with-default "Enter the second input filename: " nil "~/Downloads/"))
         (output (fm/read-file-name-with-default "Enter the output filename: " nil (if buffer (concat (file-name-sans-extension buffer) ".pdf") "~/"))))
    (async-shell-command (concat "pdfunite " input1 " " input2 " " output))))

(defun fm/split-pdf-files()
  "Prompt the user to enter filenames and display the pdfunite command."
  (interactive)
  (let* ((buffer buffer-file-name)
         (input1 (fm/read-file-name-with-default "Enter the first input filename: " nil (if buffer (concat (file-name-sans-extension buffer) ".pdf") "~/")))
         (output (file-name-sans-extension (fm/read-file-name-with-default "Enter the output filename: " nil (if buffer buffer "~/")))))
    (async-shell-command (format "pdfseparate %s %s%%d.pdf" input1 output))))

(defun fm/compress-pdf-files()
  "Compress PDF files"
  (interactive)
  (let* ((input (read-file-name "Enter the input filename: "))
         (output (fm/read-file-name-with-default "Enter the output filename: " nil (concat (file-name-sans-extension input) "_compressed.pdf"))))
    (async-shell-command (concat "magick -density 168x168 -quality 76 -compress JPEG " input " " output))
    (if (y-or-n-p "Delete original file?")
        (delete-file input))))

(defun fm/compress-marked-pdf-files ()
  "Compress marked PDF files in Dired using ImageMagick."
  (interactive)
  (let* ((marked-files (dired-get-marked-files))
         (output-prefix "_compressed.pdf"))
    (dolist (input marked-files)
      (let* ((output (fm/read-file-name-with-default "Enter the output filename: " nil (concat (file-name-sans-extension input) output-prefix))))
        (async-shell-command (concat "magick -density 168x168 -quality 76 -compress JPEG " input " " output))
        (if (y-or-n-p (format "Delete original file %s?" input))
            (delete-file input))))))

(use-package request
  :straight t)

(use-package a
  :straight t)

;; Check if a backup folder exists, if not, makes one.
(defvar --backup-directory (concat user-emacs-directory "backups"))
(if (not (file-exists-p --backup-directory))
    (make-directory --backup-directory t))

(setq backup-directory-alist `(("." . ,--backup-directory)))

(setq backup-by-copying t)

(setq make-backup-files t               ; backup of a file the first time it is saved.
      version-control t                 ; version numbers for backup files
      delete-old-versions t             ; delete excess backup files silently
      kept-old-versions 6               ; oldest versions to keep when a new numbered backup is made (default: 2)
      kept-new-versions 9               ; newest versions to keep when a new numbered backup is made (default: 2)
      )

(defvar --autosaves-directory (concat user-emacs-directory "autosaves" "/"))
(if (not (file-exists-p --autosaves-directory))
    (make-directory --autosaves-directory t))

(setq auto-save-file-name-transforms
      `((".*" ,--autosaves-directory t)))

(setq auto-save-default t               ; auto-save every buffer that visits a file
      auto-save-timeout 20              ; number of seconds idle time before auto-save (default: 30)
      auto-save-interval 200            ; number of keystrokes between auto-saves (default: 300)
      )

(global-set-key (kbd "<escape>") 'keyboard-escape-quit)

(define-key global-map (kbd "C-c j")
	    (lambda () (interactive) (org-capture nil "jj")))

(use-package general
  :config
  (general-create-definer fm/leader-keys
    :keymaps '(normal insert visual emacs)
    :prefix "SPC"
    :global-prefix "C-SPC")
  (fm/leader-keys
    "t"  '(:ignore t :which-key "toggles")
    "ts" '(hydra-text-scale/body :which-key "scale text")

    ;; Smartparens bindings
    "s"  '(:ignore s :which-key "smartparens")
    "si" '(sp-beginning-of-sexp :which-key "Beginning of SEXP")
    "sa" '(sp-end-of-sexp :which-key "End of SEXP")
    "sd"'(sp-down-sexp :which-key "Move down expression")
    "su"'(sp-up-sexp :which-key "Move up expression")
    "sk" '(sp-forward-slurp-sexp :which-key "Forward Slurp")
    "sj" '(sp-backward-slurp-sexp :which-key "Backward Slurp")
    "sl" '(sp-backward-barf-sexp :which-key "Backward Barf")
    "sh" '(sp-backward-barf-sexp :which-key "Backward Barf")
    "s)" '(sp-wrap-round :which-key "Wrap sexp with ()")
    "s}" '(sp-wrap-curly :which-key "Wrap sexp with {}")
    "s]" '(sp-wrap-square :which-key "Wrap sexp with []")
    ;; Avy bindings

    "f"  '(:ignore s :which-key "Avy")
    "ff" '(avy-goto-char :which-key "Find Char")
    "fg" '(avy-goto-char-2 :which-key "Find 2 Chars")
    "fl" '(avy-goto-line :which-key "Line")
    "fg" '(avy-goto-char-2 :which-key "Find 2 Characters")
    "fo" '(avy-org-goto-heading-timer :which-key "Org Heading")
    ;;"p" '(projectile-command-map :which-key "Projectile Commands")
    "u" '(undo-fu-only-undo :which-key "undo" )
    "r" '(undo-fu-only-redo :which-key "redo" )
    "TAB" '(org-cycle :which-key "Cycle Heading Visibility" )
    "1" '(yas-expand :which-key "Expand snippet" )
    "q" '(evil-force-normal-state :which-key "Normal Mode")
    ))

(use-package which-key
  :init (which-key-mode)
  :diminish which-key-mode
  :config
  (setq which-key-idle-delay 0.5)
  (setq which-key-allow-evil-operators t))

(use-package evil
  :init
  (setq evil-want-integration t)
  (setq evil-want-keybinding nil)
  (setq evil-want-C-u-scroll nil)
  (setq evil-want-C-i-jump t)
  (setq evil-undo-system 'undo-fu)
  :config
  (evil-mode 1)
  (define-key evil-insert-state-map (kbd "C-g") 'evil-normal-state)
  ;; Use visual line motions even outside of visual-line-mode buffers
  (evil-global-set-key 'motion "j" 'evil-next-visual-line)
  (evil-global-set-key 'motion "k" 'evil-previous-visual-line)
  ;;Defining initial mode for evil - Insert term?
  (evil-set-initial-state 'messages-buffer-mode 'normal)
  (evil-set-initial-state 'dashboard-mode 'normal)
  )


(setq which-key-show-operator-state-maps t)

(use-package evil-collection
  :after evil
  :config
  (evil-collection-init))

(use-package evil-goggles
  :straight t
  :config
  (evil-goggles-mode)
  )

(use-package evil-commentary
  :straight t
  :config
  (evil-commentary-mode)
  )

(use-package goto-chg
  :straight t
  ;; :config
  ;; (evil-goto-chg-mode)
  )

(use-package evil-lion
  :ensure t
  :config
  (evil-lion-mode))

(use-package evil-surround
  :straight t
  :config
  (global-evil-surround-mode 1))

(add-hook 'org-mode-hook (lambda ()
                           (push '(?` . ("=" . "=")) evil-surround-pairs-alist)))

(use-package evil-owl
  :straight t
  :config
  (setq evil-owl-max-string-length 500)
  (add-to-list 'display-buffer-alist
               '("*evil-owl*"
                 (display-buffer-in-side-window)
                 (side . bottom)
                 (window-height . 0.3)))
  (evil-owl-mode))

(evil-define-key 'normal 'evil-normal-state-map
  (kbd "g r") 'revert-buffer)

(use-package hydra)

(defhydra hydra-text-scale (:timeout 4)
  "scale text"
  ("j" text-scale-increase "in")
  ("k" text-scale-decrease "out")
  ("f" nil "finished" :exit t))

(use-package ace-window
  :straight t
  :bind
  ("M-o" . ace-window)
  :config
  (setq aw-keys '(?a ?s ?d ?f ?g ?h ?k ?l))

  )

(use-package evil-snipe

    :straight t
    :config
    (evil-snipe-mode)
    (evil-snipe-override-mode)
    (add-hook 'magit-mode-hook 'turn-off-evil-snipe-override-mode)
    :custom
    (evil-snipe-scope 'whole-buffer)
    )

  (evil-define-key '(normal motion) evil-snipe-mode-map
                                          ;"s" 'evil-snipe-s
    "s" 'evil-substitute
    "S" 'evil-avy-goto-char-2)
;; (evil-define-key 'operator evil-snipe-mode-map
  ;;   "z" 'evil-snipe-s
  ;;   "Z" 'evil-snipe-S
  ;;   "x" 'evil-snipe-x
  ;;   "X" 'evil-snipe-X)

  (evil-define-key 'motion evil-snipe-override-mode-map
    "f" 'evil-snipe-f
    "F" 'evil-snipe-F
    "t" 'evil-snipe-t
    "T" 'evil-snipe-T)

  ;; Haven't found this to be very useful
  ;; (when evil-snipe-override-evil-repeat-keys
  ;;   (evil-define-key 'motion map
  ;;     ";" 'evil-snipe-repeat
  ;;     "," 'evil-snipe-repeat-reverse))

(use-package avy
  :straight t
  :config
  (setq avy-background t)
  (setq avy-all-windows t)
  :bind
  ("M-f" . avy-goto-char)
  )

(global-set-key (kbd "C-x h") 'previous-buffer)
(global-set-key (kbd "C-x l") 'next-buffer)

(use-package dogears
  ;; :quelpa (dogears :fetcher github :repo "alphapapa/dogears.el"
  ;; :files (:defaults (:exclude "helm-dogears.el")))

  ;; These bindings are optional, of course:
  :bind (:map global-map
              ("M-g d" . dogears-go)
              ("M-g M-b" . dogears-back)
              ("M-g M-f" . dogears-forward)
              ("M-g M-d" . dogears-list)
              ("M-g M-D" . dogears-sidebar))
  :config (dogears-mode)
  (setq dogears-functions '()))

(use-package outli
  :straight (outli :type git :host github :repo "jdtsmith/outli")
  :bind (:map outli-mode-map ; convenience key to get back to containing heading
   	      ("C-c C-p" . (lambda () (interactive) (outline-back-to-heading))))
  :hook ((prog-mode) . outli-mode)
  :config
  (setq outli-blend 0)
  (setq outli-heading-config 
	'((clojure-mode ";; " ?# t nil)
	  (emacs-lisp-mode ";;" 59 t)
	  (tex-mode "%%" 37 t)
	  (org-mode)
	  (t
	   (let*
	       ((c
		 (or comment-start "#"))
		(space
		 (unless
		     (eq
		      (aref c
			    (1-
			     (length c)))
		      32)
		   " ")))
	     (concat c space))
	   42))

	)
  (dolist (face '((outline-1 . 1.2)
                  (outline-2 . 1.1)
                  (outline-3 . 1.05)
                  (outline-4 . 1.0)
                  (outline-5 . 1.1)
                  (outline-6 . 1.1)
                  (outline-7 . 1.1)
                  (outline-8 . 1.1)))
    (set-face-attribute (car face) nil :font "Cantarell" :weight 'regular :height (cdr face)))
  )

(use-package smartparens
  :straight t
  :config (progn (show-smartparens-global-mode t))
  )
(add-hook 'clojure-mode-hook 'smartparens-mode)
(add-hook 'prog-mode-hook 'smartparens-mode)
;;(add-hook 'org-mode-hook 'smartparens-mode) ;; This sometimes caused issues with unmatched parens

(defun my-angle-bracket-fix ()
  (modify-syntax-entry ?< "w")
  (modify-syntax-entry ?> "w"))

(add-hook 'org-mode-hook 'my-angle-bracket-fix)

;(use-package dired-open
;  :config
;  ;; Doesn't work as expected!
;  ;;(add-to-list 'dired-open-functions #'dired-open-xdg t)
;  (setq dired-open-extensions '(("png" . "feh")
;                                ("mkv" . "mpv"))))

(use-package dirvish
  :straight t
  :custom
  ;; Go back home? Just press `bh'
  (dirvish-header-line-format '(:left (path) :right (free-space)))
  (dirvish-mode-line-format ; it's ok to place string inside
   '(:left (sort file-time " " file-size symlink) :right (omit yank index)))
  ;; Don't worry, Dirvish is still performant even you enable all these attributes
  (dirvish-attributes '(all-the-icons file-size collapse subtree-state vc-state))
  ;; Maybe the icons are too big to your eyes
  ;; (dirvish-all-the-icons-height 0.8)
  ;; In case you want the details at startup like `dired'
  ;; (dirvish-hide-details nil)
  (dirvish-bookmark-entries
   `(("h" "~/"              "Home")
     ("m" "/mnt/"           "Drives")
     ("c" "~/.config/"      "Config folder")
     ("d" "~/dotfiles/"     "Dotfiles")
     ("h" "~/homelab/"      "Homelab")
     ("p" "~/Projects"      "Projects")
     ("a" "~/Ansible/"      "Ansible")
     ("r" "~/Org-Roam"      "Org-Roam")
     ("o" "~/Documents/"    "Documents")
     ("l" "~/Downloads/"    "Downloads")
     ("e" ,user-emacs-directory "Emacs user directory"))
   )
  :config
  (evil-collection-define-key 'normal 'dired-mode-map
    "l" 'dired-find-file)
  (evil-collection-define-key 'normal 'dired-mode-map
    "h" 'dired-up-directory)
  (evil-collection-define-key 'normal 'dired-mode-map
    "?" 'dirvish-dispatch)
  (evil-collection-define-key 'normal 'dired-mode-map
    "b" 'dirvish-bookmark-jump)
  (evil-collection-define-key 'normal 'dired-mode-map
    (kbd "<tab>")  'dirvish-subtree-toggle)
  (evil-collection-define-key 'normal 'dired-mode-map
    "p"  'dirvish-history-jump)
  (evil-collection-define-key 'normal 'dired-mode-map
    "*"    'dirvish-mark-menu)
  (evil-collection-define-key 'normal 'dired-mode-map
    "E"    'dirvish-emerge-menu)
  (evil-collection-define-key 'normal 'dired-mode-map
    "N"    'dirvish-narrow)
  (evil-collection-define-key 'normal 'dired-mode-map
    "r"    'dirvish-fd-roam)
  (evil-collection-define-key 'normal 'dired-mode-map
    "b"    'dirvish-bookmark-jump)
  (evil-collection-define-key 'normal 'dired-mode-map
    "i"    'dirvish-file-info-menu)
  (evil-collection-define-key 'normal 'dired-mode-map
    "K"    'dired-kill-line)
  (evil-collection-define-key 'normal 'dired-mode-map
    "f"    'dirvish-fd)
  (dirvish-peek-mode)
  (dirvish-override-dired-mode)
  ;; Dired options are respected except a few exceptions,
  ;; see *In relation to Dired* section above
  (setq dired-recursive-deletes 'always)
  (setq delete-by-moving-to-trash t)
  (setq dired-dwim-target t)
  ;; Make sure to use the long name of flags when exists
  ;; eg. use "--almost-all" instead of "-A"
  ;; Otherwise some commands won't work properly
  (setq dired-listing-switches
        "-l --almost-all --human-readable --time-style=long-iso --group-directories-first --no-group")
  ;;"-l --almost-all --human-readable --time-style=long-iso --group-directories-first --no-group"

  :bind
  ;;override evil keys with =evil-collection-define-key= in the :config block
  ;; Bind `dirvish|dirvish-side|dirvish-dwim' as you see fit
  (("C-c f" . dirvish-fd)
   :map dired-mode-map ; Dirvish respects all the keybindings in this map
   ("h" . dired-up-directory)
   ("j" . dired-next-line)
   ("k" . dired-previous-line)
   ("l" . dired-find-file)
   ;; ("i" . wdired-change-to-wdired-mode)
   ;; ("." . dired-omit-mode)
   ("TAB" . dirvish-subtree-toggle)
   ("SPC" . dirvish-history-jump)
   ("M-n" . dirvish-history-go-forward)
   ("M-p" . dirvish-history-go-backward)
   ("M-s" . dirvish-setup-menu)
   ("M-f" . dirvish-toggle-fullscreen)
   ("*"   . dirvish-mark-menu)
   ("E"   . dirvish-emerge-menu)
   ("N"   . dirvish-narrow)
   ("r"   . dirvish-fd-roam)
   ("b"   . dirvish-bookmark-jump)
   ("f"   . dirvish-file-info-menu)
   ("K"   . dirvish-dispatch)
   ([remap dired-sort-toggle-or-edit] . dirvish-quicksort)
   ([remap dired-do-redisplay] . dirvish-ls-switches-menu)
   ([remap dired-summary] . dirvish-dispatch)
   ([remap dired-do-copy] . dirvish-yank-menu)
   ([remap mode-line-other-buffer] . dirvish-history-last)))


(dirvish-override-dired-mode)

(use-package dired-toggle-sudo
  :straight t
  :commands dired dirvish
  :config
  (evil-collection-define-key 'normal 'dired-mode-map
    "C-c s" 'dired-toggle-sudo))

(defun shell-double-quote (s)
  "Double quotes for the string that shall be fed to shell command"
  (replace-regexp-in-string "\"" "\\\\\"" s))

(defun ffmpeg-cut-dired ()
  "Dired function to cut video files from specific begin time for
specific duration. You can cut many files at once if marked, or
the function will work only on the specific file. Customize the
list of video extensions below."
  (interactive)
  (let* ((files (dired-get-marked-files))
	 (allowed-extensions '("mp4" "ogv" "3gp")))
    (dolist (file files)
      (let* ((file-sans-ext (file-name-sans-extension file))
             (file-ext (file-name-extension file)))
	(when (seq-contains allowed-extensions file-ext)
	  (let* ((output (concat file-sans-ext "-cut." file-ext))
		 (begin-time (read-from-minibuffer "Begin time in format 00:00:00: " "00:00:00"))
		 (duration (read-from-minibuffer "Duration in format 00:00:00: " "00:00:10"))
		 (command (format "ffmpeg -i \"%s\" -ss %s -t %s -async 1 \"%s\""
				  (shell-double-quote file)
				  begin-time duration
				  (shell-double-quote output))))
	    (shell-command command)))))))

(use-package vertico
  :straight t
  :init
  (vertico-mode)
  :bind (:map vertico-map
              ("C-j" . vertico-next)
              ("C-k" . vertico-previous)
              ("C-f" . vertico-exit-input)
              )
  :custom
  ;; Different scroll margin
  ;; (setq vertico-scroll-margin 0)

  ;; Show more candidates
  ;; (setq vertico-count 20)

  ;; Grow and shrink the Vertico minibuffer
  ;; (setq vertico-resize t)

  ;; Optionally enable cycling for `vertico-next' and `vertico-previous'.
  (vertio-cycle t)
  )




(use-package vertico-directory
  :after vertico
  :straight nil
  :bind (:map vertico-map
              ("RET" . vertico-directory-enter)
              ("M-DEL" . vertico-directory-delete-char)
              ("DEL" . fm/my-vertico-delete-word)) ; Use your own function name
  :load-path "~/.emacs.d/straight/repos/vertico/extensions/"
  ;; Tidy shadowed file names
  :hook (rfn-eshadow-update-overlay . vertico-directory-tidy))



(defun fm/my-vertico-delete-word ()
  "Delete a word in the minibuffer during file path completion and a char when completing anyting else."
  (interactive)
  (if (and (bound-and-true-p vertico--directory)
           (string-match-p "\\`[ \t]*\\([^ \t]+\\)" (minibuffer-contents)))
      (call-interactively #'backward-delete-word)
    (vertico-directory-delete-char 1)))


;; Persist history over Emacs restarts. Vertico sorts by history position.
(use-package savehist
  :init
  (savehist-mode))

;; A few more useful configurations copied from the repo...
(use-package emacs
  :init

  ;;VERTICO
  ;; Configuration copied from the repo, don't know whatfor
  ;; Add prompt indicator to `completing-read-multiple'.
  ;; We display [CRM<separator>], e.g., [CRM,] if the separator is a comma.
  (defun crm-indicator (args)
    (cons (format "[CRM%s] %s"
                  (replace-regexp-in-string
                   "\\`\\[.*?]\\*\\|\\[.*?]\\*\\'" ""
                   crm-separator)
                  (car args))
          (cdr args)))
  (advice-add #'completing-read-multiple :filter-args #'crm-indicator)

  ;; Do not allow the cursor in the minibuffer prompt
  (setq minibuffer-prompt-properties
        '(read-only t cursor-intangible t face minibuffer-prompt))
  (add-hook 'minibuffer-setup-hook #'cursor-intangible-mode)

  ;; Emacs 28: Hide commands in M-x which do not work in the current mode.
  ;; Vertico commands are hidden in normal buffers.
  ;; (setq read-extended-command-predicate
  ;;       #'command-completion-default-include-p)

  ;; Enable recursive minibuffers
  (setq enable-recursive-minibuffers t)

  ;;CORFU
  ;; Enable indentation+completion using the TAB key.
  ;; `completion-at-point' is often bound to M-TAB.
  (setq tab-always-indent 'complete)
  )

;; Optionally use the `orderless' completion style.
(use-package orderless
  :straight t
  :init
  ;; Configure a custom style dispatcher (see the Consult wiki)
  ;; (setq orderless-style-dispatchers '(+orderless-dispatch)
  ;;       orderless-component-separator #'orderless-escapable-split-on-space)
  (setq completion-styles '(orderless basic)
        completion-category-defaults nil
        completion-category-overrides '((file (styles partial-completion))))
  )

(use-package marginalia
  :straight t
  ;; Either bind `marginalia-cycle` globally or only in the minibuffer
  :bind ( :map minibuffer-local-map
          ("M-A" . marginalia-cycle))
  :after vertico
  :init
  (marginalia-mode))

(use-package consult

  :straight t
  ;; Replace bindings. Lazily loaded due by `use-package'.
  :bind (;; C-c bindings (mode-specific-map)
         ("C-c h" . consult-history)
         ("C-c m" . consult-mode-command)
         ("C-c k" . consult-kmacro)
         ;; C-x bindings (ctl-x-map)
         ("C-x b" . consult-buffer)                ;; orig. switch-to-buffer
         ("C-x p b" . consult-project-buffer)      ;; orig. project-switch-to-buffer
         ;; Custom M-# bindings for fast register access
         ("M-#" . consult-register-load)
         ("M-'" . consult-register-store)          ;; orig. abbrev-prefix-mark (unrelated)
         ("C-M-#" . consult-register)
         ;; Other custom bindings
         ("M-y" . consult-yank-pop)                ;; orig. yank-pop
         ("<help> a" . consult-apropos)            ;; orig. apropos-command
         ;; M-g bindings (goto-map)
         ("M-g e" . consult-compile-error)
	 ("M-g F" . consult-flymake)
         ("M-g f" . consult-flycheck)               ;; Alternative: consult-flycheck
         ("M-g g" . consult-goto-line)             ;; orig. goto-line
         ("M-g M-g" . consult-goto-line)           ;; orig. goto-line
         ("M-g o" . consult-outline)               ;; Alternative: consult-org-heading
         ("M-g h" . consult-org-heading)               ;; Alternative: consult-org-heading
         ("M-g m" . consult-mark)
         ("M-g k" . consult-global-mark)
         ("M-g i" . consult-imenu)
         ("M-g I" . consult-imenu-multi)
         ;; M-s bindings (search-map)
         ("M-s d" . consult-find)
         ("M-s D" . consult-locate)
         ("M-s g" . consult-grep)
         ("M-s G" . consult-git-grep)
         ("M-s r" . consult-ripgrep)
         ("M-s l" . consult-line)
         ("C-s" . consult-line)             ;; orig. goto-line
         ("M-s L" . consult-line-multi)
         ("M-s m" . consult-multi-occur)
         ("M-s k" . consult-keep-lines)
         ("M-s u" . consult-focus-lines)
         ;; Isearch integration
         ("M-s e" . consult-isearch-history)
         :map isearch-mode-map
         ("M-e" . consult-isearch-history)         ;; orig. isearch-edit-string
         ("M-s e" . consult-isearch-history)       ;; orig. isearch-edit-string
         ("M-s l" . consult-line)                  ;; needed by consult-line to detect isearch
         ("M-s L" . consult-line-multi)            ;; needed by consult-line to detect isearch
         ;; Minibuffer history
         :map minibuffer-local-map
         ("M-s" . consult-history)                 ;; orig. next-matching-history-element
         ("M-r" . consult-history))                ;; orig. previous-matching-history-element

  ;; Enable automatic preview at point in the *Completions* buffer. This is
  ;; relevant when you use the default completion UI.
  :hook (completion-list-mode . consult-preview-at-point-mode)

  ;; The :init configuration is always executed (Not lazy)
  :init

  ;; Optionally configure the register formatting. This improves the register
  ;; preview for `consult-register', `consult-register-load',
  ;; `consult-register-store' and the Emacs built-ins.
  (setq register-preview-delay 0.5
        register-preview-function #'consult-register-format)

  ;; Optionally tweak the register preview window.
  ;; This adds thin lines, sorting and hides the mode line of the window.
  (advice-add #'register-preview :override #'consult-register-window)

  ;; Use Consult to select xref locations with preview
  (setq xref-show-xrefs-function #'consult-xref
        xref-show-definitions-function #'consult-xref)

  ;; Configure other variables and modes in the :config section,
  ;; after lazily loading the package.
  :config


  ;; Optionally configure the narrowing key.
  ;; Both  and C-+ work reasonably well.
  (setq consult-narrow-key "<") ;; (kbd "C-+")

  ;; Optionally make narrowing help available in the minibuffer.
  ;; You may want to use `embark-prefix-help-command' or which-key instead.
  ;; (define-key consult-narrow-map (vconcat consult-narrow-key "?") #'consult-narrow-help)

  ;; By default `consult-project-function' uses `project-root' from project.el.
  ;; Optionally configure a different project root function.
  ;; There are multiple reasonable alternatives to chose from.
      ;;;; 1. project.el (the default)
  ;; (setq consult-project-function #'consult--default-project--function)
      ;;;; 2. projectile.el (projectile-project-root)
  ;; (autoload 'projectile-project-root "projectile")
  ;; (setq consult-project-function (lambda (_) (projectile-project-root)))
      ;;;; 3. vc.el (vc-root-dir)
  ;; (setq consult-project-function (lambda (_) (vc-root-dir)))
      ;;;; 4. locate-dominating-file
  ;; (setq consult-project-function (lambda (_) (locate-dominating-file "." ".git")))
  )

(use-package consult-yasnippet
  :straight t
  :bind
  ("M-s y" . consult-yasnippet)
  )



(use-package embark
  :straight t

  :bind
  (("C-$" . embark-act)         ;; pick some comfortable binding
   ("M-." . embark-dwim)        ;; good alternative: M-.
   ("C-h B" . embark-bindings)) ;; alternative for `describe-bindings'

  :init

  ;; Optionally replace the key help with a completing-read interface
  (setq prefix-help-command #'embark-prefix-help-command)

  :config

  ;; Hide the mode line of the Embark live/completions buffers
  (add-to-list 'display-buffer-alist
               '("\\`\\*Embark Collect \\(Live\\|Completions\\)\\*"
                 nil
                 (window-parameters (mode-line-format . none))))
  )

;; Consult users will also want the embark-consult package.
(use-package embark-consult
  :straight t
  :after (embark consult)
  :demand t ; only necessary if you have the hook below
  ;; if you want to have consult previews as you move around an
  ;; auto-updating embark collect buffer
  :hook
  (embark-collect-mode . consult-preview-at-point-mode))

(use-package wgrep
  :straight t)

(use-package corfu
   :straight t
   :load-path "straight/repos/corfu/extensions"
   ;; Optional customizations
   :custom
   ;; (corfu-cycle t)                ;; Enable cycling for `corfu-next/previous'
   (corfu-auto t)                 ;; Enable auto completion
   (corfu-separator ?\s)          ;; Orderless field separator
   ;; (corfu-quit-no-match nil)      ;; Never quit, even if there is no match
   ;; (corfu-preview-current nil)    ;; Disable current candidate preview
   (corfu-preselect-first nil)    ;; Disable candidate preselection
   ;; (corfu-on-exact-match 'quit)     ;; Configure handling of exact matches
   (corfu-echo-documentation t) ;; Disable documentation in the echo area
   (corfu-auto-prefix 4)
   (corfu-quit-at-boundary 'separator) 
   (corfu-quit-no-match 'separator) 
   ;; (corfu-scroll-margin 5)        ;; Use scroll margin

   ;; Enable Corfu only for certain modes.
   ;; :hook ((prog-mode . corfu-mode)
   ;;        (shell-mode . corfu-mode)
   ;;        (eshell-mode . corfu-mode))
   :bind (:map corfu-map
("SPC" . corfu-insert-separator)
               ("C-j" . corfu-next)
               ("C-k" . corfu-previous)
               ("C-f" . corfu-insert))
   :init
   (require 'corfu-popupinfo)
   (setq corfu-popupinfo-delay 2)
   (corfu-popupinfo-mode)
   (global-corfu-mode)
   )

(use-package kind-icon
  :after corfu
  :custom
  (kind-icon-use-icons t)
  (kind-icon-default-face 'corfu-default) ; Have background color be the same as `corfu' face background
  (kind-icon-blend-background nil)  ; Use midpoint color between foreground and background colors ("blended")?
  (kind-icon-blend-frac 0.08)
  :config
  (add-to-list 'corfu-margin-formatters #'kind-icon-margin-formatter) ; Enable `kind-icon'
  )

(use-package cape
  ;; Bind dedicated completion commands
  ;; Alternative prefix keys: C-c p, M-p, M-+, ...
  :bind (("M-p p" . completion-at-point) ;; capf
         ("M-p t" . complete-tag)        ;; etags
         ("M-p d" . cape-dabbrev)        ;; or dabbrev-completion
         ("M-p h" . cape-history)
         ("M-p f" . cape-file)
         ("M-p k" . cape-keyword)
         ("M-p s" . cape-symbol)
         ("M-p a" . cape-abbrev)
         ("M-p i" . cape-ispell)
         ("M-p l" . cape-line)
         ("M-p w" . cape-dict)
         ("M-p \\" . cape-tex)
         ("M-p _" . cape-tex)
         ("M-p ^" . cape-tex)
         ("M-p &" . cape-sgml)
         ("M-p r" . cape-rfc1345))
  :init
  ;; Add `completion-at-point-functions', used by `completion-at-point'.
  (add-to-list 'completion-at-point-functions #'cape-file)
  (add-to-list 'completion-at-point-functions #'cape-dabbrev)
  (add-to-list 'completion-at-point-functions #'cape-history)
  (add-to-list 'completion-at-point-functions #'cape-keyword)
  ;;(add-to-list 'completion-at-point-functions #'cape-tex)
  ;;(add-to-list 'completion-at-point-functions #'cape-sgml)
  ;;(add-to-list 'completion-at-point-functions #'cape-rfc1345)
  ;;(add-to-list 'completion-at-point-functions #'cape-abbrev)
  ;;(add-to-list 'completion-at-point-functions #'cape-ispell)
  ;; (add-to-list 'completion-at-point-functions #'cape-dict)
  (add-hook 'org-mode-hook (lambda () (add-to-list 'completion-at-point-functions #'cape-symbol)))
  ;;(add-to-list 'completion-at-point-functions #'cape-line)
  )



(use-package yasnippet)

(add-hook 'lua-mode-hook 'yas-minor-mode)
(add-hook 'org-mode-hook 'yas-minor-mode)
(add-hook 'yaml-mode-hook 'yas-minor-mode)
(add-hook 'tex-mode-hook 'yas-minor-mode)

(use-package auto-yasnippet
  :straight t
:bind 
("C-c C-y w" . aya-create)
("C-c C-y TAB" . aya-expand)
("C-c C-y SPC" . aya-expand-from-history)
("C-c C-y d" . aya-delete-from-history)
("C-c C-y c" . aya-clear-history)
("C-c C-y n" . aya-next-in-history)
("C-c C-y p" . aya-previous-in-history)
("C-c C-y s" . aya-persist-snippet)
("C-c C-y o" . aya-open-line))

(use-package magit
  :custom
  (magit-display-buffer-function #'magit-display-buffer-same-window-except-diff-v1))

(use-package projectile
  :straight t
  :diminish projectile-mode
                                        ;:init
                                        ;(projectile-mode +1)
  :custom ((projectile-completion-system 'ivy))
  :bind-keymap
  ("C-c p" . projectile-command-map)
  ;;:init
  ;;In the following, you can define a folder to look for projects
  ;;NOTE: Set this to the folder where you keep your Git repos!
  ;; (when (file-directory-p "~/Projects/Code")
  ;; (setq projectile-project-search-path '("~/Projects/Code")))
  ;;(setq projectile-switch-project-action #'projectile-dired)
  )

(use-package counsel-projectile
  :straight t
                                        ;:config counsel-projectile-mode t
  )

(use-package dashboard
  :straight t
  :config
  (setq dashboard-center-content nil)
  (setq dashboard-icon-type 'all-the-icons) 
  (setq dashboard-vertically-center-content nil)
  (dashboard-setup-startup-hook))

(setq dashboard-startup-banner 'logo)

(setq dashboard-items '(;;(recents  . 10)
                        (bookmarks . 5)
                                        ;(projects . 5)
                        (agenda . 5)
                                        ;(registers . 5)
                        ))

(setq initial-buffer-choice (lambda () (get-buffer-create "*dashboard*")))

(defun fm/org-mode-setup ()
  (org-indent-mode)
  ;;(variable-pitch-mode 1)
  (visual-line-mode 1))

(defun fm/org-font-setup ()
  ;; Replace list hyphen with dot
  (font-lock-add-keywords 'org-mode
                          '(("^ *\\([-]\\) "
                             (0 (prog1 () (compose-region (match-beginning 1) (match-end 1) "•"))))))


  ;; Set faces(=font) for heading levels
  (dolist (face '((org-level-1 . 1.2)
                  (org-level-2 . 1.1)
                  (org-level-3 . 1.05)
                  (org-level-4 . 1.0)
                  (org-level-5 . 1.1)
                  (org-level-6 . 1.1)
                  (org-level-7 . 1.1)
                  (org-level-8 . 1.1)))
    (set-face-attribute (car face) nil :font "Cantarell" :weight 'regular :height (cdr face)))

  ;; Ensure that anything that should be fixed-pitch in Org files appears that way
  (set-face-attribute 'org-block nil :foreground nil :inherit 'fixed-pitch)
  (set-face-attribute 'org-code nil   :inherit '(shadow fixed-pitch))
  (set-face-attribute 'org-table nil   :inherit '(shadow fixed-pitch))
  (set-face-attribute 'org-verbatim nil :inherit '(shadow fixed-pitch))
  (set-face-attribute 'org-special-keyword nil :inherit '(font-lock-comment-face fixed-pitch))
  (set-face-attribute 'org-meta-line nil :inherit '(font-lock-comment-face fixed-pitch))
  (set-face-attribute 'org-checkbox nil :inherit 'fixed-pitch))

(use-package org
  :hook (org-mode . fm/org-mode-setup)
  :bind (("C-<" . org-agenda)
         :map org-agenda-mode-map
         ("C-j" . org-agenda-next-line)
         ("C-k" . org-agenda-previous-line))
  
  :config
  (setq org-ellipsis " ▾") ;;Character to denote a heading with content
  (setq org-return-follows-link t) 
  (setq org-attach-preferred-new-method 'dir)
  (setq org-priority-lowest ?F)
  (setq org-agenda-start-with-log-mode t) ;;Enable log mode
  (setq org-log-done 'time) ;; On setting a task to done, a timestamp is created(Other Options available)
  (setq org-log-into-drawer t)
  (fm/org-font-setup)
  (setq org-agenda-files
        '("~/OrgRoam/"))
  ;;Allow for refiling of tasks
  (setq org-refile-targets
        '(("Archive.org" :maxlevel . 1)
          ("Tasks.org" :maxlevel . 1)))
  ;; Save Org buffers after refiling!
  (advice-add 'org-refile :after 'org-save-all-org-buffers)

  ;;Todo State keywords | = seperates active and passive states
  (setq org-todo-keywords
        '((sequence "TODO(t)" "NEXT(n)" "|" "DONE(d!)")))

(setq org-tag-alist
      '((:startgroup)
					; Put mutually exclusive tags here
        ("Personal" . ?p)
        ("Schreinerei" . ?s)
        (:endgroup)
        ("Kundenauftrag" . ?k)
        ("Administrativ" . ?a)
        ("Urgent" . ?u)
        ("Important" . ?i)
        ("Intrinsic" . ?I)
        ("Extrinsic" . ?e)
        ("Today" . ?t)
        ("Week" . ?w)
        ("Month" . ?m)
        ("Langfristig" . ?l)
        ("Gesundheit" . ?g)))
  ;; Configure custom agenda views
  (setq org-agenda-custom-commands
        '(("d" "Dashboard"
           ((agenda "" ((org-deadline-warning-days 7)))
            (todo "NEXT"
                  ((org-agenda-overriding-header "Next Tasks")))
            (tags-todo "agenda/ACTIVE" ((org-agenda-overriding-header "Active Projects")))))

          ("n" "Next Tasks"
           ((todo "NEXT"
                  ((org-agenda-overriding-header "Next Tasks")))))

          ;;Adding agenda custom view based on tags
          ;;Syntax: Add tags by starting them with "+", remove them with "-"
          ("W" "Work Tasks" tags-todo "+work-email")

          ;;Custom view on different ptoperties
          ;; Low-effort next actions
          ("e" tags-todo "+TODO=\"NEXT\"+Effort<15&+Effort>0"
           ((org-agenda-overriding-header "Low Effort Tasks")
            (org-agenda-max-todos 20)
            (org-agenda-files org-agenda-files)))

          ))


  ;;Capture Template
  (setq org-capture-templates
        `(("t" "Tasks / Projects")
          ("tt" "Task" entry (file+olp "~/Documents/OrgFiles/tasks.org" "Inbox")
           "* TODO %?\n  %U\n  %a\n  %i" :empty-lines 1)
          ;;"%?" = Cursor placement in template
          ;;"%u" = Timestamp
          ;;"%a" or "%i" (?) = link to where your at in emacs

          ("j" "Journal Entries")
          ("jj" "Journal" entry
           (file+olp+datetree "~/Documents/OrgFiles/Journal.org")
           "\n* %<%I:%M %p> - Journal :journal:\n\n%?\n\n"
           ;; ,(dw/read-file-as-string "~/Notes/Templates/Daily.org")
           :clock-in :clock-resume
           :empty-lines 1)
          ("jm" "Meeting" entry
           (file+olp+datetree "~/Documents/OrgFiles/Journal.org")
           "* %<%I:%M %p> - %a :meetings:\n\n%?\n\n"
           :clock-in :clock-resume
           :empty-lines 1)

          ("w" "Workflows")
          ("we" "Checking Email" entry (file+olp+datetree "~/Documents/OrgFiles/Journal.org")
           "* Checking Email :email:\n\n%?" :clock-in :clock-resume :empty-lines 1)

          ))
  (require 'org-habit)
  (add-to-list 'org-modules 'org-habit)
  (setq org-habit-graph-column 60)
  )

(use-package org-bullets
  :after org
  :hook (org-mode . org-bullets-mode)
  :custom
  (org-bullets-bullet-list '("◉" "○" "●" "○" "●" "○" "●")))

(defun fm/org-mode-visual-fill ()
  (setq visual-fill-column-width 110
        visual-fill-column-center-text t)
  (visual-fill-column-mode 1))
(use-package visual-fill-column
  :hook (org-mode . fm/org-mode-visual-fill))

(org-babel-do-load-languages
 'org-babel-load-languages
 '((emacs-lisp . t)
   (latex . t)
   (lua . t)
   (scheme . t)
   (python . t)
   (clojure . t)
   (shell . t)))

(push '("conf-unix" . conf-unix) org-src-lang-modes)
(push '("yaml" . yaml) org-src-lang-modes)
(push '("clojure" . clojure) org-src-lang-modes)
(push '("lua" . lua) org-src-lang-modes)
(push '("html" . html) org-src-lang-modes)

(require 'org-tempo)
(add-to-list 'org-structure-template-alist '("sh" . "src shell"))
(add-to-list 'org-structure-template-alist '("el" . "src emacs-lisp"))
(add-to-list 'org-structure-template-alist '("py" . "src python"))
(add-to-list 'org-structure-template-alist '("ya" . "src yaml"))
(add-to-list 'org-structure-template-alist '("lua" . "src lua"))
(add-to-list 'org-structure-template-alist '("clj" . "src clojure"))
(add-to-list 'org-structure-template-alist '("tex" . "src latex"))
(add-to-list 'org-structure-template-alist '("cfg" . "src conf-unix"))
(add-to-list 'org-structure-template-alist '("lax" . "EXPORT latex"))
(add-to-list 'org-structure-template-alist '("scm" . "src scheme"))

(use-package eval-in-repl
  :straight t)
;;(define-key clojure-mode-map (kbd "<C-return>") 'eir-eval-in-cider)

(use-package org-babel-eval-in-repl
  :straight t
  :config
  (setq eir-jump-after-eval nil))

(define-key org-mode-map (kbd "M-c") 'ober-eval-in-repl)
(define-key org-mode-map (kbd "M-C") 'ober-eval-block-in-repl)

(defun tangle-on-save-org-mode-file ()
  (when (and (string-match-p
              (regexp-quote ".org") (message "%s" (current-buffer)))
             (not (string-match-p
                   (regexp-quote "[") (message "%s" (current-buffer)))))
    (org-babel-tangle)))

(add-hook 'after-save-hook 'tangle-on-save-org-mode-file)

(use-package ox-reveal
  :straight t
  :config
  (setq org-reveal-root "file:///home/fm/.emacs.d/reveal.js-master")
  )

(use-package org-ql
  :straight t)

(defun my-org-link-eval (path &rest _rest)
  "Evaluate PATH and return result as string."
  (condition-case err
      (prin1-to-string (eval (read path)))
    (error (format "Error in eval of %S: %S." path err))))

(defun my-org-link-eval-activate (start end path bracketp)
  "Display text from START to END as result of the eval of PATH.
BRACKETP is ignored."
  (save-excursion
    (if org-descriptive-links
	(add-text-properties
	 start end
	 (list 'display (propertize (my-org-link-eval path) 'face 'org-link)))
      (remove-text-properties start end '(display nil)))))

(org-link-set-parameters "val"
			 :export #'my-org-link-eval
			 :activate-func #'my-org-link-eval-activate)

(defun org-elisp-link-follow (path)
  "Evaluate the Elisp function in PATH and display the result."
  (let ((result (eval (read path))))
    (message "%s" result)))

(defun org-elisp-link-toggle-display ()
  "Toggle display of Elisp link results in the current buffer."
  (interactive)
  (org-with-wide-buffer
   (goto-char (point-min))
   (while (re-search-forward "\\[\\[elisp:\\(.*?\\)\\]\\[\\(.*?\\)\\]\\]" nil t)
     (let ((elisp (match-string 1))
           (desc (match-string 2)))
       (if (get-text-property (match-beginning 0) 'org-link-elisp-evaluated)
           (progn
             (remove-text-properties (match-beginning 0) (match-end 0) '(display nil org-link-elisp-evaluated nil)))
         (let ((result (format "%s" (eval (read elisp)))))
           (add-text-properties (match-beginning 0) (match-end 0)
                                `(display ,result org-link-elisp-evaluated t))))))))

(org-link-set-parameters "elisp"
                         :follow #'org-elisp-link-follow)

;; Bind the toggle function to a key in Org mode
;;(define-key org-mode-map (kbd "C-c C-x C-l") 'org-elisp-link-toggle-display)

(use-package org-super-agenda
  :straight t
  :config

  (org-super-agenda-mode)
  (setq org-super-agenda-groups
        '(;; Each group has an implicit boolean OR operator between its selectors.
          (:name "Today"  ; Optionally specify section name
                 :todo "TODO")
          (:name "Urgent"
                 :todo "TODO"
                 :priority "A")
          (:name "Important"
                 :todo "TODO"
  		 :scheduled nil
                 :tag "Important"
                 :priority>= "C")
          (:name "Personal"
                 :todo "TODO"
  		 :scheduled nil
                 :tag "Personal"
                 :priority>= "B")
          (:name "Waiting"
                 :todo "TODO"
  		 :scheduled nil
  		 :todo "WAITING"
  		 :order 8)
          (:name "Low Priority"
                 :todo "TODO"
  		 :scheduled nil
		 :priority<= "C"
		 :order 9)
  	  )
        ))
;; After the last group, the agenda will display items that didn't
;; match any of these groups, with the default order position of 99

(defun fm/org-insert-image ()
  "Prompt for an image, caption, alignment, and width, and insert the Org mode syntax for an image."
  (interactive)
  (let* ((image-path (read-file-name "Select image file: "))
         (caption (read-string "Enter caption: "))
         (alignment (completing-read "Select alignment: " '("center" "left" "right") nil t "center"))
	 (width (read-number "Enter width in decimals(Such as 0.5, 0.7,1): " 1))
         (org-image-string 
          (concat (format "#+CAPTION: %s\n" caption)
                  (format "#+ATTR_LATEX: :float wrap  :width %s\\textwidth \n"  width)
                  (format "[[file:%s]]" image-path))))
    (insert org-image-string)))

(use-package org-roam
  :straight t
  :demand t
  :init
  (setq org-roam-v2-ack t)
  (require 'org-roam-node)
  :custom
  (org-roam-directory "~/OrgRoam")
  (org-roam-completion-everywhere t)
  (org-roam-capture-templates
   '(("d" "default" plain
      "%?"
      :target (file+head "${slug}.org"  "#+title: ${title}\n")
      :unnarrowed t)

     ("L" "letter-english" plain
      (file "/home/fm/OrgRoam/Capture-Templates/fm_letter_template.org")
      :target (file+head "${slug}.org"
                         "#+title: ${title}\n")
      :unnarrowed t)

     ("r" "Rechnung" plain
      (file "/home/fm/OrgRoam/Capture-Templates/rechnungneu.org")
      :target (file+head "${slug}.org"
                         "#+title: ${title}\n")
      :unnarrowed t)

     ("o" "Offerte" plain
      (file "/home/fm/OrgRoam/Capture-Templates/offertetemplate.org")
      :target (file+head "${slug}.org"
                         "#+title: ${title}\n")
      :unnarrowed t)
     
     ("l" "letter-german" plain
      (file "/home/fm/OrgRoam/Capture-Templates/fm_letter_template_german.org")
      :target (file+head "${slug}.org"
                         "#+title: ${title}\n")
      :unnarrowed t)

     ("b" "bibliography note" plain
      "%?"
      :target
      (file+head
       "%(expand-file-name (or citar-org-roam-subdir \"\") org-roam-directory)/${citar-citekey}.org"
       "#+title: ${citar-citekey} (${citar-date}). ${note-title}.\n#+created: %U\n#+last_modified: %U\n\n")
      :unnarrowed t)

     ("P" "Reveal Presentation" plain
      (file "/home/fm/OrgRoam/Capture-Templates/reveal_template.org")
      :target (file+head "${slug}.org"
                         "#+title: ${title}\n")
      :unnarrowed t)

     ("a" "Latex Artikel Deutsch" plain
      (file "/home/fm/OrgRoam/Capture-Templates/artikel-template.org")
      :target (file+head "${slug}.org"
                         "#+title: ${title}\n")
      :unnarrowed t)

     ("A" "Latex Article English" plain
      (file "/home/fm/OrgRoam/Capture-Templates/article-template.org")
      :target (file+head "${slug}.org"
                         "#+title: ${title}\n")
      :unnarrowed t)

     ("k" "Client/Contact" plain
      (file "/home/fm/OrgRoam/Capture-Templates/client-template.org")
      :target (file+head "${slug}.org"
                         "#+title: ${title}\n")
      :unnarrowed t)

     ("p" "Auftrag/Projekt Schreinerei" plain
      (file "/home/fm/OrgRoam/Capture-Templates/auftrag-template.org")
      :target (file+head "${slug}.org"
                         "#+title: ${title}\n")
      :unnarrowed t)
     ("K" "Project" plain "* Goals\n\n%?\n\n* Tasks\n\n** TODO %^{Add initial tasks}\n\n* Dates\n\n"
      :if-new (file+head "${slug}.org" "#+title: ${title}\n#+date: %U\n#+filetags: Task")
      :unnarrowed t)

     ("t" "Task" plain "* Goals\n\n%?\n\n* Tasks\n\n** TODO %^{Add initial tasks}\n\n* Dates\n\n"
      :if-new (file+head "${slug}.org" "#+title: ${title}\n#+date: %U\n#+filetags: Task")
      :unnarrowed t)

     ("R" "receipe" plain (file "~/OrgRoam/Capture-Templates/receipes.org")
      :if-new (file+head "${slug}.org" "#+title: ${title}\n#+filetags: Receipe")
      :unnarrowed t)

     ("c" "CAD" plain
      (file "/home/fm/.emacs.d/roam-cad-template-clj.org")
      :if-new (file+head "${slug}.org"
                         ":PROPERTIES:\n:PRJ-NAME: ${slug}\n:END:\n#+title: ${title}\n")
      :unnarrowed t)
     ))
  (org-roam-dailies-capture-templates
   '(("d" "default" entry "* %<%I:%M %p>: %?"
      :if-new (file+head "%<%Y-%m-%d>.org" "#+title: %<%Y-%m-%d>\n"))))
  :bind (("C-c n l" . org-roam-buffer-toggle)
         ("C-c n f" . org-roam-node-find)
         ("C-c n a" . org-roam-tag-add)
         ("C-c n i" . org-roam-node-insert)
         :map org-mode-map
         ("C-M-i" . completion-at-point))
  :bind-keymap
  ("C-c d" . org-roam-dailies-map)
  :config
  (org-roam-db-autosync-mode)
  (require 'org-roam-dailies) ;;Ensure that this keymap is available
  (setq org-roam-node-display-template 
        (concat "${title:*} "
                (propertize "${tags:20}" 'face 'org-tag))) ;; Show tags when searching for notes
  (org-roam-setup))

(defun fm/org-roam-capture-with-prompt (prompt &optional default-value)
  (interactive "sPrompt: \n")
  (let* ((default-value (or default-value ""))
	 (input (completing-read (concat prompt " (default " default-value "): ") nil))
	 (trimmed-input (if (string-empty-p input) default-value input)))
    trimmed-input))

(defun fm/org-roam-filter-by-tag (tag-name)
   (lambda (node)
     (member tag-name (org-roam-node-tags node))))

 (defun fm/org-roam-list-notes-by-tag (tag-name)
   (mapcar #'org-roam-node-file
           (seq-filter
            (fm/org-roam-filter-by-tag tag-name)
            (org-roam-node-list))))

 (defun fm/org-roam-refresh-agenda-list ()
   (interactive)
   (setq org-agenda-files (fm/org-roam-list-notes-by-tag "Task")))

 ;; Build the agenda list the first time for the session
;; (fm/org-roam-refresh-agenda-list)

(defun fm/org-roam-project-finalize-hook ()
  "Adds the captured project file to `org-agenda-files' if the
capture was not aborted."
  ;; Remove the hook since it was added temporarily
  (remove-hook 'org-capture-after-finalize-hook #'fm/org-roam-project-finalize-hook)

  ;; Add project file to the agenda list if the capture was confirmed
  (unless org-note-abort
    (with-current-buffer (org-capture-get :buffer)
      (add-to-list 'org-agenda-files (buffer-file-name)))))

(defun fm/org-roam-find-task ()
  (interactive)
  ;; Add the project file to the agenda after capture is finished
  (add-hook 'org-capture-after-finalize-hook #'fm/org-roam-project-finalize-hook)

  ;; Select a project file to open, creating it if necessary
  (org-roam-node-find
   nil
   nil
   (fm/org-roam-filter-by-tag "Task")
   :templates
   '(("t" "Task" plain "* Goals\n\n%?\n\n* Tasks\n\n** TODO %^{Add initial tasks}\n\n* Dates\n\n"
      :if-new (file+head "%<%Y%m%d%H%M%S>-${slug}.org" "#+title: ${title}\n#+category: ${title}\n#+filetags: Task")
      :unnarrowed t))))

(global-set-key (kbd "C-c n t") #'fm/org-roam-find-task)

(defun fm/org-roam-capture-inbox ()
  (interactive)
  (org-roam-capture- :node (org-roam-node-create)
                     :templates '(("i" "inbox" plain "* %?"
                                   :if-new (file+head "Inbox.org" "#+title: Inbox\n#+filetags: Task")))))

(global-set-key (kbd "C-c n n") #'fm/org-roam-capture-inbox)

(defun fm/org-roam-capture-task ()
  (interactive)
  ;; Add the project file to the agenda after capture is finished
  (add-hook 'org-capture-after-finalize-hook #'fm/org-roam-project-finalize-hook)

  ;; Capture the new task, creating the project file if necessary
  (org-roam-capture- :node (org-roam-node-read
                            nil
                            (fm/org-roam-filter-by-tag "Task"))
                     :templates '(("p" "project" plain "** TODO %?"
                                   :if-new (file+head+olp "%<%Y%m%d%H%M%S>-${slug}.org"
                                                          "#+title: ${title}\n#+category: ${title}\n#+filetags: Task"
                                                          ("Tasks"))))))

(global-set-key (kbd "C-c n p") #'fm/org-roam-capture-task)

(use-package org-roam-ui
  :straight
  (:host github :repo "org-roam/org-roam-ui" :branch "main" :files ("*.el" "out"))
  :after org-roam
  ;;         normally we'd recommend hooking orui after org-roam, but since org-roam does not have
  ;;         a hookable mode anymore, you're advised to pick something yourself
  ;;         if you don't care about startup time, use
  ;;  :hook (after-init . org-roam-ui-mode)
  :config
  (setq org-roam-ui-sync-theme t
        org-roam-ui-follow t
        org-roam-ui-update-on-save t
        org-roam-ui-open-on-start t))

(use-package consult-org-roam
  :straight t
  :init
  (require 'consult-org-roam)
  ;; Activate the minor-mode
  (consult-org-roam-mode 1)
  :custom
  (consult-org-roam-grep-func #'consult-ripgrep)
  :config
  ;; Eventually suppress previewing for certain functions
  (consult-customize
   consult-org-roam-forward-links
   :preview-key (kbd "M-."))
  :bind
  ("C-c n e" . consult-org-roam-file-find)
  ("C-c n l" . consult-org-roam-forward-links)
  ("C-c n b" . consult-org-roam-backlinks)
  ("C-c n s" . consult-org-roam-search))

(use-package citar
  :straight t
  :no-require
  :custom
  (org-cite-global-bibliography '("~/OrgRoam/bib/references.bib"))
  (org-cite-insert-processor 'citar)
  (org-cite-follow-processor 'citar)
  (org-cite-activate-processor 'citar)
  (citar-bibliography org-cite-global-bibliography)
  ;; optional: org-cite-insert is also bound to C-c C-x C-@
  :bind
  (:map org-mode-map :package org ("C-c b" . #'org-cite-insert))
  :hook
  (LaTeX-mode . citar-capf-setup)
  (org-mode . citar-capf-setup))

(use-package citar-embark
  :after citar embark
  :no-require
  :config (citar-embark-mode))

(setq citar-at-point-function 'embark-act)

(use-package org-roam-bibtex
  :straight t
  :after org-roam)

(use-package citar-org-roam
  :after (citar org-roam)
  :config (citar-org-roam-mode))

(setq citar-org-roam-capture-template-key "b")

(use-package ebib
  :straight (:includes ebib-biblio)
  :config
  (setq ebib-preload-bib-files '("~/OrgRoam/bib/references.bib"))
  (setq ebib-file-search-dirs '("~/OrgRoam/bib/sources/"))
  (setq ebib-preload-bib-files nil)
  (setq ebib-bibtex-dialect 'biblatex)
  (setq ebib-use-timestamp t)
  (setq ebib-file-associations nil)
  (add-hook 'ebib-index-mode-hook 'turn-off-evil-mode)
  )

(use-package ebib-biblio
  :after (ebib biblio)
  :bind (:map ebib-index-mode-map
              ("B" . ebib-biblio-import-doi)
              :map biblio-selection-mode-map
              ("e" . ebib-biblio-selection-import)))

(use-package biblio
  :straight t
  :config
  (setq biblio-download-directory '"~/OrgRoam/bib/sources/"))

(use-package org-noter
  :straight t
  :config
  (evil-collection-define-key 'normal 'org-noter-doc-mode-map
    "i"  'org-noter-insert-note))

(use-package ledger-mode
  :straight t
  :config
  (setq org-date-display-format "%Y/%m/%d")
  (add-to-list 'auto-mode-alist '("\\.ledger$" . ledger-mode)
               (setq ledger-accounts-file '"~/OrgRoam/schreinermueller.ledger"))
  (setq ledger-reports '(("bal" "%(binary) -f %(ledger-file) bal")
("Assets & Liabilities" "%(binary) -f %(ledger-file)  bal ^assets ^liabilities")
("Revenue & Expense" "%(binary) -f %(ledger-file)  bal ^revenue ^expenses")
			 ("reg" "%(binary) -f %(ledger-file) reg")
			 ("payee" "%(binary) -f %(ledger-file) reg @%(payee)")
			 ("account" "%(binary) -f %(ledger-file) reg %(account)"))))

(defun fm/add-ledger-transaction ()
  "Prompt the user for transaction details and append them to a file."
  (interactive)
  (let* ((date (org-read-date))
         (year (nth 5 (org-parse-time-string date)))
         (receipt-dir (format "~/OrgRoam/data/quittungen/%s" year))
         (transaction-name (read-string "Enter payment name: "))
         (uuid (uuidgen-1))
         (receipt-file (fm/add-receipt))
	 rg
         (new-receipt-file (when receipt-file (concat receipt-dir "_" transaction-name "_" uuid ".pdf")))
         (or-node (consult-org-roam-node-read))
         (or-node-title (org-roam-node-title or-node))
         (or-node-id (org-roam-node-id or-node))
         (account1 (completing-read "Enter debit: " (ledger-accounts-list)))
         (account2 (completing-read "Enter credit: " (ledger-accounts-list)))
         (amount (read-number "Enter Amount: "))

         (ledger-transaction-content (format "%s %s \n    %s   %s\n    %s\n" date transaction-name account1 amount account2))
         (fmdb-transaction-content (format "{:xt/id \"%s\" :name \"transaction-%s\" :type :transaction :trn/orgnode-title \"%s\" :trn/orgnode-id \"%s\" :trn/date \"%s\" :trn/name \"%s\" :trn/aktivkonto \"%s\" :trn/passivkonto  \"%s\" :trn/summe \"%s\" :trn/quittung \"%s\"}" uuid transaction-name or-node-title or-node-id date transaction-name account1 account2 amount (if new-receipt-file new-receipt-file "")))
         (ledger-file-path ledger-accounts-file) ; Replace with the actual file path
         (fmdb-file-path "~/projects/fmdb/docs/ledger.edn")
         (fmdb-confirmation (fmdb/ingest-single fmdb-transaction-content)))
    (when receipt-file
      (copy-file receipt-file new-receipt-file))
    (with-temp-buffer
      (insert ledger-transaction-content)
      (append-to-file (point-min) (point-max) ledger-file-path))
    (with-temp-buffer
      (insert fmdb-transaction-content)
      (append-to-file (point-min) (point-max) fmdb-file-path))
    (message "Transaction details: %s, %s " ledger-transaction-content fmdb-confirmation)))

(defun fm/add-receipt ()
  "Prompt user to decide if they want to add a receipt file."
  (interactive)
  (let ((add-receipt (y-or-n-p "Do you want to add a receipt file? ")))
    (if add-receipt
        (let ((receipt-file (read-file-name "Quittung hinzufügen: ")))
          ;; (message "Selected file: %s" receipt-file)
          receipt-file
          ;; Perform further actions with the selected file, e.g., saving it, processing it, etc.
          )

      ;; (message "No receipt file added")
      )))
;; (message (fm/add-receipt))

(defun fm/replace-words-in-file (filename replacements)
        "Replace words in FILENAME according to the REPLACEMENTS alist."
        (with-temp-buffer
          (insert-file-contents filename)
          (dolist (replacement replacements)
            (goto-char (point-min))
            (while (search-forward (car replacement) nil t)
              (replace-match (cdr replacement) nil t)))
          (write-region (point-min) (point-max) filename)))

(defun fm/ledger-ger-to-en ()
  "Change the account names in a ledger file from German to English"
  (interactive)
  (let* ((filename (read-file-name "Enter ledger file: "))
         (replacements '(("Aktiven" . "Assets")
                         ("Passiven" . "Liabilities")
                         ("Aufwand" . "Expenses")
                         ("Ertrag" . "Revenue")
                         ("Eigenkapital" . "Equity"))))
    (fm/replace-words-in-file filename replacements)))

(defun fm/ledger-en-to-ger ()
  "Change the account names in a ledger file from English to German"
  (interactive)
  (let* ((filename (read-file-name "Enter ledger file: "))
         (replacements '(("Assets" . "Aktiven")
                         ("Liabilities" . "Passiven")
                         ("Expenses" . "Aufwand")
                         ("Revenue" . "Ertrag")
                         ("Equity" . "Eigenkapital"))))
    (fm/replace-words-in-file filename replacements)))

;; (use-package casual
  ;; :straight t
  ;; :bind (:map calc-mode-map ("C-q" . 'casual-main-menu)))

(use-package leo
  :straight t)

(defun fm/lsp-mode-setup ()
  (setq lsp-headerline-breadcrumb-segments '(path-up-to-project file symbols))
  (lsp-headerline-breadcrumb-mode))

(use-package lsp-mode
  :commands
  (lsp lsp-deferred)
  :init
  (setq lsp-keymap-prefix "C-l")  ;; Or 'C-l', 's-l'
  (defun fm/lsp-mode-setup-completion ()
    (setf (alist-get 'styles (alist-get 'lsp-capf completion-category-defaults))
          '(orderless))) ;; Configure orderless as a filtering mechanism for completion
  :custom
  (lsp-completion-provider :none) ;; we use Corfu!
  :hook
  (lsp-mode . fm/lsp-mode-setup)
  (sh-mode . lsp-deferred)
  (lsp-completion-mode . fm/lsp-mode-setup-completion)
  :config
  (setq gc-cons-threshold 100000000)
  (setq read-process-output-max (* 1024 1024))
  (lsp-enable-which-key-integration t)
  )

(use-package lsp-treemacs
  :after lsp)

(use-package dap-mode
  ;; Uncomment the config below if you want all UI panes to be hidden by default!
  ;; :custom
  ;; (lsp-enable-dap-auto-configure nil)
  :config
  (dap-ui-mode 1)
  ;; Bind `C-l d` to `dap-hydra` for easy access
  (general-define-key
   :keymaps 'lsp-mode-map
   :prefix lsp-keymap-prefix
   "d" '(dap-hydra t :wk "debugger"))
  )

;; (use-package treesit-auto
;; :straight t
;;   :custom
;;   (treesit-auto-install 'prompt)
;;   :config
;;   (treesit-auto-add-to-auto-mode-alist 'all)
;;   (global-treesit-auto-mode))

(use-package csv-mode
  :straight t
  :mode "\\.csv\\'"
  :hook (lua-mode . lsp-deferred)
  )

(use-package lua-mode
  :mode "\\.lua\\'"
  :hook (lua-mode . lsp-deferred)
  )

(use-package typescript-mode
  :mode "\\.ts\\'"
  :hook (typescript-mode . lsp-deferred)
  :config
  (setq typescript-indent-level 2)
  (require 'dap-node) ;; Set up Node debugging
  (dap-node-setup) ;; Automatically installs Node debug adapter if needed
  )

(use-package python-mode
  :hook (python-mode . lsp-deferred)
  :custom
  (python-shell-interpreter "python3")
  (dap-python-interpreter "python3")
  (dap-python-debugger 'debugpy)
  :config
  (require 'dap-python)
  )

(use-package pyvenv
  :config
  (pyvenv-mode 1))

(use-package zencoding-mode
  :straight t)
;;(add-to-list 'auto-mode-alist '("\\.yml\\'" . yaml-mode))

(use-package yaml-mode)
(add-to-list 'auto-mode-alist '("\\.yml\\'" . yaml-mode))

(use-package geiser-guile :straight t)

(use-package clojure-mode
  :straight t
  :hook (clojure-mode . lsp-deferred)
  )

(use-package cider
  :straight t
  :hook
  (cider-repl-mode . smartparens-mode)
  (cider-mode . smartparens-mode)
  :config
  (setq cider-eldoc-display-for-symbol-at-point nil)
  )

(use-package clj-deps-new
  :straight t
  :after clojure-mode
  )

(setq org-babel-clojure-backend 'cider
      org-babel-clojure-sync-nrepl-timeout nil)

(defun cider-jack-in-babashka (&optional project-dir)
  "Start a utility CIDER REPL backed by Babashka, not related to a specific project."
  (interactive)
  (lexical-let ((project-dir (or project-dir (project-root (project-current t)))))
    (nrepl-start-server-process
     project-dir
     "bb --nrepl-server 0"
     (lambda (server-buffer)
       (cider-nrepl-connect
        (list :repl-buffer server-buffer
              :repl-type 'clj
              :host (plist-get nrepl-endpoint :host)
              :port (plist-get nrepl-endpoint :port)
              :project-dir project-dir
              :session-name "babashka"
              :repl-init-function (lambda ()
                                    (setq-local cljr-suppress-no-project-warning t
                                                cljr-suppress-middleware-warnings t)
                                    (rename-buffer "*babashka-repl*"))))))))

(use-package clomacs
  :straight t
  )

(use-package auctex
  :straight t
  :config
  (setq TeX-engine 'luatex)

  )

(add-to-list 'load-path "~/.emacs.d/straight/repos/org/lisp/")
(eval-after-load 'ox '(require 'ox-koma-letter))

(with-eval-after-load "ox-latex"
  (add-to-list 'org-latex-classes
               '("scrartcl" "\\documentclass{scrartcl}"
                 ("\\section{%s}" . "\\section*{%s}")
                 ("\\subsection{%s}" . "\\subsection*{%s}")
                 ("\\subsubsection{%s}" . "\\subsubsection*{%s}")
                 ("\\paragraph{%s}" . "\\paragraph*{%s}")
                 ("\\subparagraph{%s}" . "\\subparagraph*{%s}"))))

(eval-after-load 'ox-koma-letter
  '(progn
     (add-to-list 'org-latex-classes
                  '("fm-letter-english" "\\documentclass\[%
     DIV=14\]\{scrlttr2\}"))
     (add-to-list 'org-latex-classes
                  '("fm-rechnung" "\\documentclass\[
     DIV=14,fromalign=center,fromrule=afteraddress,
fromphone,fromemail]\{scrlttr2\}"))

     (setq org-koma-letter-default-class "fm-letter-english")))

(defun fm/generate-latex-list-from-region ()
  "Generate LaTeX code for a list from the Org list in the marked region."
  (interactive)
  (let* ((region-text (buffer-substring (region-beginning) (region-end)))
         (lines (split-string region-text "\n" t " "))
         (type (if (string-match "^\\s-*-\\s-*\\[\\s-*\\]\\s-*" region-text) 'checkboxes
                 (if (string-match "^\\s-*[0-9]+\\.\\s-*" region-text) 'numbered 'unordered)))
         (list-items (pcase type
                       ('checkboxes (mapcar (lambda (line) (replace-regexp-in-string "^\\s-*-\\s-*\\[\\s-*\\]\\s-*" "" line)) lines))
                       ('numbered (mapcar (lambda (line) (replace-regexp-in-string "^\\s-*[0-9]+\\.\\s-*" "" line)) lines))
                       ('unordered (mapcar (lambda (line) (replace-regexp-in-string "^\\s-*[-+*]\\s-*" "" line)) lines))))
         (half-length (/ (length list-items) 2))
         (first-column (if (eq (% (length list-items) 2) 0)
                           (seq-take list-items half-length)
                         (seq-take list-items (1+ half-length))))
         (second-column (seq-drop list-items (length first-column))))
    (message "Detected list type: %s" type)
    (insert "#+BEGIN_EXPORT latex\n")
    (insert "\\begin{minipage}[t]{0.45\\textwidth}\n")
    (insert (format "\\begin{%s}\n" (pcase type ('checkboxes "itemize") ('numbered "enumerate") ('unordered "itemize"))))
    (dolist (item first-column)
      (insert (format "    \\item %s%s\n"
                      (if (eq type 'checkboxes) "[{$\\square$}] " "")
                      item)))
    (insert (format "\\end{%s}\n" (pcase type ('checkboxes "itemize") ('numbered "enumerate") ('unordered "itemize"))))
    (insert "\\end{minipage}\n\\hfill\n")
    (insert "\\begin{minipage}[t]{0.45\\textwidth}\n")
    (insert (format "\\begin{%s}\n" (pcase type ('checkboxes "itemize") ('numbered "enumerate") ('unordered "itemize"))))
    (dolist (item second-column)
      (insert (format "    \\item %s%s\n"
                      (if (eq type 'checkboxes) "[{$\\square$}] " "")
                      item)))
    (insert (format "\\end{%s}\n" (pcase type ('checkboxes "itemize") ('numbered "enumerate") ('unordered "itemize"))))
    (insert "\\end{minipage}\n")
    (insert "#+END_EXPORT\n")))

(setq lsp-tex-server 'digestif)
(add-hook 'LaTeX-mode-hook 'lsp-deferred)

(use-package lsp-ltex
  :straight t

  ;; :hook
  ;; (text-mode . (lambda ()
  ;;                (require 'lsp-ltex)
  ;;                (lsp)))  ; or lsp-deferred
  ;; (tex-mode . (lambda ()
  ;;                 (require 'lsp-ltex)
  ;;                 (lsp)))
  ;; (org-mode . (lambda ()
  ;;              (require 'lsp-ltex)
  ;;            (lsp)))

  :config
  (add-to-list 'lsp-language-id-configuration '(org-mode . "org"))
  (add-to-list 'lsp-language-id-configuration '(tex-mode . "latex/p"))

  (setq lsp-ltex-language "de-CH")
  ;;  :custom
  ;;(lsp-ltex-language "auto")
  ;;(lsp-ltex-language "en-US")
  )

(use-package flycheck
  :init
  (global-flycheck-mode))

(use-package tablist
  :straight t)

(use-package pdf-tools
  :ensure t
  :config
  (pdf-tools-install)
  (setq org-noter-highlight-selected-text t)
  (setq-default pdf-view-display-size 'fit-page))

(use-package nov
  :straight t
  :config
  (add-to-list 'auto-mode-alist '("\\.epub\\'" . nov-mode)))

(use-package ellama
  :straight t
  :init
  (setopt ellama-language "English")
  (require 'llm-ollama)
  (setopt ellama-provider
	  (make-llm-ollama
	   :chat-model "zephyr" :embedding-model "zephyr")))

(defun efs/configure-eshell ()
  ;; Save command history when commands are entered
  (add-hook 'eshell-pre-command-hook 'eshell-save-some-history)

  ;; Truncate buffer for performance
  (add-to-list 'eshell-output-filter-functions 'eshell-truncate-buffer)

  ;; Bind some useful keys for evil-mode
  (evil-define-key '(normal insert visual) eshell-mode-map (kbd "<home>") 'eshell-bol)
  (evil-normalize-keymaps)

  (setq eshell-history-size         10000
        eshell-buffer-maximum-lines 10000
        eshell-hist-ignoredups t
        eshell-scroll-to-bottom-on-input t))

(use-package eshell
  :hook (eshell-first-time-mode . efs/configure-eshell))

(use-package eshell-git-prompt

  :config
  (eshell-git-prompt-use-theme 'powerline))

(defun fmdb/init ()
  "Initialize fmdb"
  (interactive)
  ;; (add-to-list 'load-path "~/.emacs.d/fmdb/src/elisp/")
  (add-to-list 'load-path "~/projects/fmdb/src/elisp/")
  (require 'fmdb)
  (fmdb/fmdb-init)
  )
;; (fmdb/init)
;; # This is a tet
;; (fmdb/return-all)

(defun fm/create-project-node ()
  "Prompt for project information and create a new Org Roam node using the existing 'p' template, inserting client info."
  (interactive)
  (let* ((project-name (read-string "Enter Project Name: "))
         (project-type (read-string "Enter Project Type: "))
         (client-name (read-string "Enter Client Name: "))
         ;; Org roam capture template: 'p' is assumed to already exist.
         (template-name "p")
         ;; Set up org-roam-capture info with dynamically provided data
         (org-roam-capture-templates
          (list (assoc template-name org-roam-capture-templates)))
         (org-roam-capture--info `(:title ,project-name :template ,template-name)))
    ;; Use Org Roam to capture a new node with the 'p' template
    (org-roam-capture-
     :node (org-roam-node-create :title project-name)
     :info org-roam-capture--info
     :props '(:finalize find-file))
    ;; Insert the client name into the "* Klient" section
    (goto-char (point-min)) ;; Go to the start of the buffer
    (when (search-forward "* Klient" nil t) ;; Find the "* Klient" section
      (forward-line 1) ;; Move to the next line after "* Klient"
      (insert client-name "\n")))) ;; Insert the client name

;; To use the function, you can run M-x fm/create-table-entry-hours-worked
(defun fm/org-roam-node-list-titles ()
  "Return a list of titles of all Org Roam nodes."
  (mapcar #'org-roam-node-title (org-roam-node-list)))

(defun fm/org-roam-find-node-by-title (title)
  "Find Org Roam node ID and title by its TITLE."
  (let ((nodes (org-roam-node-list))
        (found-node nil))
    (while nodes
      (let ((node (car nodes)))
        (when (string= title (org-roam-node-title node))
          (setq found-node node
                nodes nil)))
      (setq nodes (cdr nodes)))
    found-node))

(defun fm/create-table-entry-hours-worked ()
  "Prompt the user for information and create a table entry."
  (interactive)
  (let ((date (org-read-date nil nil nil "Select a date")))
    (while (progn
             (let* ((value (read-string "Stundenanzahl: "))
                    (client-title-list (fm/org-roam-node-list-titles))
                    (client-title (completing-read "Select client: " client-title-list))
                    (client-info (fm/org-roam-find-node-by-title client-title))
                    (client (if client-info
                                (format "[[id:%s][%s]]" (org-roam-node-id client-info) (org-roam-node-title client-info))
                              client-title))
                    (project-title-list (fm/org-roam-node-list-titles))
                    (project-title (completing-read "Select project: " project-title-list))
                    (project-info (fm/org-roam-find-node-by-title project-title))
                    (project (if project-info
                                 (format "[[id:%s][%s]]" (org-roam-node-id project-info) (org-roam-node-title project-info))
                               project-title))
                    (activity (read-string "Aktivität: "))
                    (status-options '("Bezahlt" "Ausstehend" "Rechnung" "Fehler" "Unbezahlt" "Keine Angabe"))
                    (status (completing-read "Status: " status-options)))
               (insert (format "| %s | %s | %s | %s | %s | %s |\n"
                               date value client project activity status)))
             (y-or-n-p "Do you want to add another entry for the same date?")))
    (insert "|-\n")))

(use-package guix
  :straight t)

(use-package all-the-icons
  :straight t
  :if (display-graphic-p)
  :commands all-the-icons-install-fonts
  :init
  (unless (find-font (font-spec :name "all-the-icons"))
    (all-the-icons-install-fonts t)))

(use-package doom-modeline
  :straight t
  :init (doom-modeline-mode 1)
  :custom ((doom-modeline-height 15)))

(use-package doom-themes
  :init (load-theme 'doom-oceanic-next t))
                                        ;Custom background
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(default ((t (:background "#263238")))))
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(safe-local-variable-values '((visual-fill-column-mode))))

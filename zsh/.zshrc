
# The following lines were added by compinstall

zstyle ':completion:*' completer _complete _ignored
zstyle ':completion:*' select-prompt %SScrolling active: current selection at %p%s
zstyle :compinstall filename '/home/fm/.zshrc'

# End of lines added by compinstall

#Enable Colours and chenga prompt
autoload -U colors && colors

# Autocompletion
autoload -Uz compinit
zstyle ':completion:*' menu select=5   #Create Selection menu for Autocomplete (Press Tab twice)
zstyle ':completion:*' matcher-list 'm:{[:lower:]}={[:upper:]}'   #Case insensitive
zstyle ':completion:*' list-colors ''  #Selection menu is coloured
setopt COMPLETE_ALIASES  
compinit
_comp_options+=(globdots)   #include hidden files

# History
HISTFILE=~/.config/zsh/histfile
HISTSIZE=5000
SAVEHIST=10000


setopt autocd
unsetopt beep

#prompt
autoload -Uz promptinit
promptinit
prompt redhat #selected prompt


#Autorun
neofetch

#vi-Mode

bindkey -v
export KEYTIMEOUT=1

#Aliases

alias ls='ls --color=auto'
alias config='/usr/bin/git --git-dir=$HOME/.dotfiles/ --work-tree=$HOME'
alias configpush='/usr/bin/git --git-dir=$HOME/.dotfiles/ --work-tree=$HOME push git@gitlab.com:MuellerF/dotfiles.git'
alias gitdot='git@gitlab.com:MuellerF/dotfiles.git'
alias vim='nvim' 
alias sshn='git@gitlab.com:MuellerF/neovim.git'
alias ssha='git@gitlab.com:MuellerF/awesomewm.git'
alias update='sudo pacman -Sy archlinux-keyring && sudo pacman -Syu'

#Only works with trash-d!
alias rm='trash'
#Automatically create a directory in .dotfiles with the same directorytructure for Gnu Stow. Only works if the file in question is in a folder in .config

alias dirstow='mkdir -p $(pwd | awk -F "/" '{print "/home/fm/.dotfiles/"$5"/"$4"/"$5}')'

PATH=$PATH:$HOME/.local:$HOME/.local/bin:

export npm_config_prefix="$HOME/.local"
export PATH

# Set twofinger click to context click
xinput set-prop "Microsoft Surface Keyboard Touchpad" "libinput Click Method Enabled" 0 1
# Turn on numlock
numlockx

# Load plugins
 
source /usr/share/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
source /usr/share/zsh/plugins/zsh-autosuggestions/zsh-autosuggestions.zsh

eval "$(starship init zsh)"

alias luamake=/home/fm/.local/bin/lua-language-server/3rd/luamake/luamake

#!/usr/bin/env bash
  
#Script to automatically create the directory structure in dotfiles necessary for Gnu Stow to create appropriat Symlinks

#The script works for files that are located within $HOME and 5 layers deeper (eg. $Home/example/example/example/example/example).

#Originally used shellcommand: mkdir -p "$(pwd | awk -F "/" '{print "$HOME/dotfiles/"$5"/"$4"/"$5}')"

#dirdepth describes the depth of the directory structure by counting the fields in $pwd

dirdepth=$(pwd | awk -F "/" '{print NF}')

# Test for dirdepth
# echo $dirdepth


#If the file is not in a folder, create a folder by specifyieng a name as a first argument 
dirname=$1

#Test for $dirname
#echo $dirname

### SETTING THE TARGET DIRECTORY###

if [ $dirdepth = 3 ]; then
	
	#echo "three"

	targetdir=$HOME/dotfiles/$dirname

elif [ $dirdepth = 4 ]; then
	
	#echo "four"

	targetdir=$HOME/dotfiles/$dirname/$(pwd | awk -F "/" '{print $4}')

elif [ $dirdepth = 5 ]; then

	#echo "five"

	targetdir=$(pwd | awk 'BEGIN{FS="/"; OFS="/"} {print $1, $2, $3, "dotfiles", $5, $4, $5}')

elif [ $dirdepth = 6 ]; then
	#echo "six"

	targetdir=$(pwd | awk 'BEGIN{FS="/"; OFS="/"} {print $1, $2, $3, "dotfiles", $6, $4, $5, $6}')

elif [ $dirdepth = 7 ]; then
	#echo "seven"

	targetdir=$(pwd | awk 'BEGIN{FS="/"; OFS="/"} {print $1, $2, $3, "dotfiles", $7, $4, $5, $6, $7}')

else 
	echo "DirectoryDepth error"
	exit 2
fi



#Creating an array with the output of the find command wich presents all the objects in the current directory.
#To allow for the termination of the programm, the string "Quit" is added as an option to the array.
declare -a options=("$(find -maxdepth 1 )" "Quit");

targetfiles=$(printf '%s\n' "${options[@]}" | rofi -dmenu -multi-select)

###MOVING FILES TO TARGETDIRECTORY###

if [ "$targetfiles" = "Quit" ]; then

	echo "Stowdir terminated"
	exit 3
else

	mkdir -p $targetdir
	mv $targetfiles $targetdir
	
	echo -e	"\nMoved\n\n$targetfiles\n\nto\n\n$targetdir\n"
fi

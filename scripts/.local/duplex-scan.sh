#! /usr/bin/env bash

# A script to scan both sides of a document with a scanner with an Automatic Document Feeder(ADF)
# It takes a output filename as a first argument.

# Check for the output filename argument
if [ -z "$1" ]; then
    echo "Usage: $0 <output_filename>"
    exit 1
fi

# Set the output filename
output_filename="$1"

# Create temporary directory if it doesn't exist
temp_dir=~/tmp/scan
mkdir -p "$temp_dir"

# Scan odd pages
scanimage --device "airscan:e0:Xerox(R) B225 MFP" --format tiff --batch="${temp_dir}/$(date +%Y%m%d_%H%M%S)_p%04d_scan.tiff"  --resolution 300 --source ADF && \
   convert "${temp_dir}"/*.tiff "${temp_dir}/odd-tmp.pdf" && \
   rm "${temp_dir}"/*scan.tiff

# Wait for 30 seconds (adjust if necessary)
sleep 30

# Scan even pages
scanimage --format tiff --batch="${temp_dir}/$(date +%Y%m%d_%H%M%S)_p%04d_scan.tiff"  --resolution 300 --source ADF && \
   convert "${temp_dir}"/*.tiff "${temp_dir}/even-tmp.pdf" && \
   rm "${temp_dir}"/*scan.tiff

# Reverse backside
pdftk "${temp_dir}/even-tmp.pdf" cat end-1 output "${temp_dir}/evenr.pdf"

# Collate odd and even pages
pdftk A="${temp_dir}/odd-tmp.pdf" B="${temp_dir}/evenr.pdf" shuffle A B output "$output_filename"

# Clean up temporary files
rm "${temp_dir}"/*
# rmdir "$temp_dir"

#!/usr/bin/bash

#Script to connect to networks using nmcli

# Declaring an array with all available networks and a quit option

declare -a availablenetworks=("$(nmcli d wifi list --rescan yes)" "Quit") 

# Declaring the selected network through rofi
selectednetwork=$(printf '%s\n' "${availablenetworks[@]}" |  rofi -dmenu -multi-select)
 
#Isolating SSID
selectedSSID=$(echo "$selectednetwork" | awk '{print $2}')

#Terminate script when "Quit" is selected
if [ "$selectednetwork" = "Quit" ]; then
	echo "WifiConnector terminated"
	exit 3

else
	echo "Establishing connnection to $selectedSSID"
	eval nmcli d wifi connect "$selectedSSID" -a

fi

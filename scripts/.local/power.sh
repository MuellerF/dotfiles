#!/bin/bash

# Define options for Rofi
options="Shutdown\nReboot\nCancel"

# Prompt user to select an option using Rofi
selected_option=$(echo -e "$options" | rofi -dmenu -p "Choose an option:")

# Perform action based on user selection
case "$selected_option" in
    "Shutdown")
        # Prompt user for confirmation
        confirm=$(echo -e "Yes\nNo" | rofi -dmenu -p "Are you sure you want to shutdown?")
        if [ "$confirm" = "Yes" ]; then
            poweroff
        fi
        ;;
    "Reboot")
        # Prompt user for confirmation
        confirm=$(echo -e "Yes\nNo" | rofi -dmenu -p "Are you sure you want to reboot?")
        if [ "$confirm" = "Yes" ]; then
             reboot
        fi
        ;;
    "Cancel")
        # Do nothing
        ;;
    *)
        echo "Invalid option"
        ;;
esac

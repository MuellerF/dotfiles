#!/bin/bash

# Prompt user to select a drive to mount
selected_drive=$(lsblk -d -p -o NAME,TYPE,MOUNTPOINT | grep "disk" | rofi -dmenu -p "Select a drive to mount:")

# Get the path to the selected drive
drive_path=$(echo "$selected_drive" | awk '{print $1}')

# Get the partition number of the selected drive
partition_number=$(echo "$selected_drive" | awk '{print $1}' | sed 's/.$//')

# Fully qualify the device path by including the partition number
fully_qualified_path="/dev/$partition_number"

# Use fzf to allow the user to navigate the file system and select a mount point
mount_point=$(find ~ -type d -print 2> /dev/null | fzf --height 50% --preview "ls -la {}")

# Mount the drive to the specified mount point
sudo mount "$fully_qualified_path" "$mount_point"

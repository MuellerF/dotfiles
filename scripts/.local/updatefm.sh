#!/usr/bin/env bash


#Setting the chassis variable to deferentiate between my devices. The command outputs 'desktop' or 'laptop'
chassis="$(hostnamectl chassis)"
echo $chassis
#Setting the directory for my dotfiles
dotfilespath="$HOME/dotfiles"

#Setting the directory for my Nvim config
nvimpath="$HOME/.config/nvim"

#Setting the directory  for my Awesome config
awesomepath="$HOME/.config/awesome"

#Declaring an array with the options for selection through rofi's menu
declare -a dirsoptions=("Dotfiles" "Nvim" "Awesome" "Quit");

#This variable describes the selected directories 
dirsselected=$(printf '%s\n' "${dirsoptions[@]}" | rofi -dmenu -multi-select)

#This If-Statement terminates the script if quit is selected
if [ "$dirsselected" = "Quit" ]; then
	echo "UpdateFM terminated"
	exit 2
fi

#this statement triggers if the selected dirs include "Dotfiles".
#It pulls the git repo
#concatonates the alacritty config to allow for autocolours
#depending o the chassis, it apends Xresources with resolution appropriate for the laptop
#Finally stows all the files
if [ "$(echo "$dirsselected" | grep "Dotfiles")" = "Dotfiles" ]; then
	(cd "$dotfilespath" || return ;
	git pull && echo "git Pull complete" && 
	cat "alacritty/.config/alacritty/alacritty-wo-colours.yml" "alacritty/.config/alacritty/alacritty-colours.yml" > "alacritty/.config/alacritty/alacritty.yml" && echo "Autocolours complete"
	if [ "$chassis" = "laptop" ]; then
		cat "laptop/.config/laptop/Xresources-laptop" >> "Xresources/.Xresources" && echo "Config for Laptop complete"
	elif  [ "$chassis" = "desktop" ]; then
		echo "Config for Desktop complete"
	else echo "Host Chassis not recognized"
	fi &&
	stow * && echo "Stow complete"
	echo "Dotfiles updated"
	)
fi

#Triggers when Nvim is selected
if [ "$(echo "$dirsselected" | grep "Nvim")" = "Nvim" ]; then
	(cd "$nvimpath" || return
	git pull &&
	echo "Successfully pulled Nvim git Repo"
	)
fi


#Triggers when Awesome is selected
if [ "$(echo "$dirsselected" | grep "Awesome")" = "Awesome" ]; then
	(cd "$awesomepath" || return
	git pull &&
	echo "Successfully pulled Awesome git Repo"
	)
fi





